

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Talentnook</title>
      <base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      <link rel="stylesheet" href="/tn/assets/css/bootstrap.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/owl.carousel.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/font-awesome.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/ui-screen.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/responsive-ui-screen.css" crossorigin="anonymous">
   </head>
   <body>
      <?php include('header.php'); ?>
      
      <section class="wrapper_become header-botmsapce">
         <div class="wrapper_sub_become">
            <img src="tn/assets/images/bec_slider1.jpg">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="wrapper_sub_become_text">
                        <div class="wrapper_sub_become_tx">Spread your talent. Teach children after school or on weekends.</div>
                        <div class="wrapper_sub_become_tx bec_mar">Teach at your home or their home.</div>
                        <div class="wrapper_sub_become_tx_bold">Become a Talentmaster!</div>
                        <button type="button" class="become_button">Get Started</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="wrapper_work_more">
         <div class="container">
            <div class="row">
               <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="wrapper_work_sub_more">
                     <img src="tn/assets/images/mid_icon1.png">
                     <div class="wrappre_work_title">Spread your talent</div>
                     <div class="wrapper_work_tx">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget libero ac magna varius interdum.</div>
                  </div>
               </div>
               <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 become_pad">
                  <div class="wrapper_work_sub_more">
                     <img src="tn/assets/images/mid_icon2.png">
                     <div class="wrappre_work_title">Spread your talent</div>
                     <div class="wrapper_work_tx">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget libero ac magna varius interdum.</div>
                  </div>
               </div>
               <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 become_pad2">
                  <div class="wrapper_work_sub_more">
                     <img src="tn/assets/images/mid_icon3.png">
                     <div class="wrappre_work_title">Spread your talent</div>
                     <div class="wrapper_work_tx">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget libero ac magna varius interdum.</div>
                  </div>
               </div>
               <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 become_pad3">
                  <div class="wrapper_work_sub_more">
                     <img src="tn/assets/images/mid_icon4.png">
                     <div class="wrappre_work_title">Spread your talent</div>
                     <div class="wrapper_work_tx">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget libero ac magna varius interdum.</div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="wrapper_talent_master">
         <div class="wrapper_talent_master"><img src="tn/assets/images/banner_img.jpg"></div>
      </section>

      <section class="wrapper_mid_saying">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="wrapper_title">
                     <div class="wrapper_tx">What Talentmasters are saying</div>
                  </div>
               </div>
               <div class="saying_wrapper_main">
                  <div class="syaing_wrapper">
                     <div class="wrapper_sub_Sub_saying">
                        <div class="popover top">
                           <div class="arrow"></div>
                           <div class="popover-content">
                              <p>"I am excited to join Talentnook. I wish I had something like this during the last 20 years of my teaching career. Can't wait to connect to new students!"</p>
                           </div>
                        </div>
                     </div>
                     <div class="pop_box">
                        <div class="pop_img"><img src="tn/assets/images/mid_saying1.png"></div>
                        <div class="pop_ul">
                           <div class="pop_sub_ul"><a href="#">John C.</a>  <span>|</span>  Dublin California</div>
                        </div>
                     </div>
                  </div>
                  <div class="syaing_wrapper">
                     <div class="wrapper_sub_Sub_saying">
                        <div class="popover top">
                           <div class="arrow"></div>
                           <div class="popover-content">
                              <p>"I am excited to join Talentnook. I wish I had something like this during the last 20 years of my teaching career. Can't wait to connect to new students!"</p>
                           </div>
                        </div>
                     </div>
                     <div class="pop_box">
                        <div class="pop_img"><img src="tn/assets/images/mid_saying2.png"></div>
                        <div class="pop_ul">
                           <div class="pop_sub_ul"><a href="#">Rachna M</a>  <span>|</span>  San Ramon California</div>
                        </div>
                     </div>
                  </div>
                  <div class="syaing_wrapper">
                     <div class="wrapper_sub_Sub_saying">
                        <div class="popover top">
                           <div class="arrow"></div>
                           <div class="popover-content">
                              <p>"I am excited to join Talentnook. I wish I had something like this during the last 20 years of my teaching career. Can't wait to connect to new students!"</p>
                           </div>
                        </div>
                     </div>
                     <div class="pop_box">
                        <div class="pop_img"><img src="tn/assets/images/mid_saying3.png"></div>
                        <div class="pop_ul">
                           <div class="pop_sub_ul"><a href="#">Angela H</a>  <span>|</span>  Pleasanton California</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <?php include('footer.php'); ?>
      <script src="/tn/assets/js/jquery-1.11.3.min.js" type="text/javascript"></script> 
      <script src="/tn/assets/js/bootstrap.min.js" type="text/javascript"></script> 
      <script type="text/javascript" src="/tn/assets/js/owl.carousel.js"></script> 
      <script src="/assets/js/enscroll-0.6.2.min.js"></script> 

   </body>
</html>

