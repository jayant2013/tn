

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Talentnook</title>
      <base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      <link rel="stylesheet" href="/tn/assets/css/bootstrap.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/font-awesome.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/ui-screen.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/responsive-ui-screen.css" crossorigin="anonymous">
   </head>
   <body>
      <?php include('header.php'); ?>
      <div class="talent-master-outer header-botmsapce">
         <div class="talent-master-topouter talentnook-profile">
            <div class="talent-master-btnouter">
               <div class="container">
                  <button class="btn requestbtn">request a talentnook</button>
                  <button class="btn contact-btn">Contact Malorum</button>
               </div>
            </div>
            <div class="talentmaster-banner-title-inner">
                  <div class="container">
                     <h2>Donec lobortis dolor</h2>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean consectetur felis sed</p>
                     <p>
                        <i class="fa fa-clock-o"></i>
                        Last Class On: July 5, 2017
                     </p>
                  </div>   
            </div>
            <div class="talentnook-profile-outer">
            	<div class="container">
            		<div class="talentnook-profile-topinner">
            			<div class="talentnook-profile-topprifle-curcleimg">
            				<div class="map-rightpannal-imginner">
		                        <div class="rightrating-img">
		                           <i class="fa fa-star active"></i>
		                           <i class="fa fa-star active"></i>
		                           <i class="fa fa-star"></i>
		                           <i class="fa fa-star"></i>
		                           <i class="fa fa-star"></i>
		                        </div>
		                        <div class="middle-imginner">
		                           <div class="rightpannal-middleimg">
		                           		<img class="img-responsive" src="../tn/assets/images/prof-4.png"></div>
		                        </div>
		                     </div>
            			</div>
            			<div class="talentnook-profile-left">
            				<div class="talentnook-profile-top">
            					<i class="fa fa-map-marker"></i>
            					<p>1615 Lexington Rd, Richmond, KY, 40475 </p>
            				</div>
            				<div class="talentnook-profile-linkdriving">
            					<a href=""> Driving direction</a>
            				</div>
            				<div class="talentnook-profile-hostby">
            					<span class="tnook-profilehost">Host By</span>
            					<span class="tnook-profilehost-profile">
            						<img class="img-responsive" src="tn/assets/images/prof-1.jpg">
            					</span>
            					<span class="tnook-profilehost-pname">Thomas EDward</span>
            				</div>
            			</div>
            			<div class="talentnook-profile-middle">
            				<div class="profile-middle-title">
            					Talentmaster <span>Malorum B</span>
            				</div>
            				<button type="button" class="btn viewprofile">View Profile</button>
            			</div>
            			<div class="talentnook-profile-right">
            				<div class="talentnook-profile-rght-top">
            					<i class="fa fa-map-marker"></i>
            					<p>Accepting new members</p>
            				</div>
            				<p>2 slot open out of 6</p>
            				<div class="talentnook-profile-rght-img">
            					<img src="../tn/assets/images/right-pannal-img01.png">
            					<img src="../tn/assets/images/right-pannal-img02.png">
            				</div>
            			</div>
            		</div>
            		<div class="talentnook-profile-btmimg">
            			<img class="img-responsive" src="/tn/assets/images/profile-bannerimg.jpg">
            		</div>
            	</div>
            </div>
         </div>
         <div class="fee-structure-outer">
            <div class="fee-structure">
               <div class="fee-structure-left">
                  <h4>Fee Structure</h4>
                  <div class="fees-deatisouter">
                     <div class="fees-deatis-left">
                        <h5 class="fees-subheading">Hourly Rate: </h5>
                        <div class="fees-prices-inner">
                           <span class="fees-discountprices">$45</span>
                           <span class="fees-cureentprices">$45</span>
                        </div>
                     </div>
                     <div class="fees-deatis-right">
                        <h5 class="fees-subheading">Group Discount:  </h5>
                        <div class="fees-groupcontent-outer">
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="fee-sechdule">
               <div class="fee-structure-left">
                  <h4>Schedule</h4>
                  <div class="fees-deatisouter">
                     <div class="fees-deatis-left">
                        <h5 class="fees-subheading">Everyday </h5>
                        <div class="everydayfees-prices-inner">
                           <span class="fees-every-day">11:30 AM to 12:00 PM</span>
                        </div>
                     </div>
                     <div class="fees-deatis-right">
                        <h5 class="fees-subheading">Week   </h5>
                        <div class="fees-groupcontent-outer">
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      	</div>
      <div class="photondvideos-section">
         <div class="container">
            <h2>My Policies</h2>
            <img src="/tn/assets/images/skiils-titleicon.png">
            <div class="videophoto-sec">
               <div class="phondvideo-videosection">
                  <img class="img-responsive" src="/tn/assets/images/video-img.jpg">
               </div>
               <div class="phondvideo-photosection">
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <?php include('footer.php'); ?>
      <script src="/tn/assets/js/jquery-1.11.3.min.js" type="text/javascript"></script> 
      <script src="/tn/assets/js/bootstrap.min.js" type="text/javascript"></script> 
      <script type="text/javascript" src="/tn/assets/js/owl.carousel.js"></script> 
      <script src="/assets/js/enscroll-0.6.2.min.js"></script> 
   </body>
</html>

