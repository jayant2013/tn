<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Talentnook</title>
      <base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      <link rel="stylesheet" href="/tn/assets/css/bootstrap.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/font-awesome.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/ui-screen.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/responsive-ui-screen.css" crossorigin="anonymous">
   </head>
   <body>
      <?php include('header.php'); ?>
      <div class="talent-master-outer header-botmsapce">
         <div class="talent-master-topouter">
            <div class="talent-master-btnouter">
               <div class="container">
                  <button class="btn requestbtn">request a talentnook</button>
                  <button class="btn contact-btn">Contact Malorum</button>
               </div>
            </div>
            <div class="talentmasbanner-pro-middlesec-outer">
               <div class="container">
                      <div class="master-average-inner">
                        <i class="fa fa-clock-o"></i>  
                        Average Response Time <span>2hr24</span>
                     </div>
                  <div class="talentmasbanner-pro-middlesec">
                     <div class="map-rightpannal-imginner">
                        <div class="rightrating-img">
                           <i class="fa fa-star active"></i>
                           <i class="fa fa-star active"></i>
                           <i class="fa fa-star"></i>
                           <i class="fa fa-star"></i>
                           <i class="fa fa-star"></i>
                        </div>
                        <div class="middle-imginner">
                           <div class="rightpannal-img-left">
                              <img src="../tn/assets/images/right-pannal-img01.png">
                           </div>
                           <div class="rightpannal-middleimg"><img class="img-responsive" src="../tn/assets/images/prof-4.png"></div>
                           <div class="rightpannal-rightimg"><img src="../tn/assets/images/right-pannal-img02.png"></div>
                        </div>
                     </div>
                  </div>
                  <div class="master-banner-socail">
                        <ul>
                           <li>
                              <a href="">
                                 <i class="fa fa-facebook"></i>
                              </a>
                           </li>
                           <li>
                              <a href="">
                                 <i class="fa fa-twitter"></i>
                              </a>
                           </li>
                           <li>
                              <a href="">
                                 <i class="fa fa-linkedin"></i>
                              </a>
                           </li>
                           <li>
                              <a href="">
                                 <i class="fa fa-instagram"></i>
                              </a>
                           </li>
                           <li>
                              <a href="">
                                 <i class="fa fa-snapchat-ghost"></i>
                              </a>
                           </li>
                           <li>
                              <a href="">
                                 <i class="fa fa-yelp"></i>
                              </a>
                           </li>
                        </ul>
                  </div>
               </div>
            </div>
            <div class="talentmaster-banner-title-inner">
                  <div class="container">
                     <h2>Malorum BCC</h2>
                     <p>Lorem Ipsum is simply dummy text of the printing </p>
                     <p>
                        <i class="fa fa-map-marker"></i>
                        Richmond, KY, 40475 <span>New Yourk</span>
                     </p>
                  </div>   
            </div>
         </div>
         <div class="student-hoursly-rate-outer">
            <div class="container">
               <div class="student-hoursly-rate-left">
                  <div class="student-hour-inner">
                     <i class="fa fa-usd"></i>
                     Hourly Rate:
                     <span class="student-discoutprice">$45</span>
                     <span class="student-curremtprice">$40</span>
                  </div>
               </div>
               <div class="student-hoursly-rate-left">
                  <div class="student-hour-inner">
                     <i class="fa fa-map-signs"></i>Within Your Area Range
                  </div>
               </div>
            </div>
         </div>
         
         <!-- <div  class="student-degree-outer">
            <div class="container">
               <div class="student-details-left">

                  <div class="heading"><div><h4>Skills & Experience</h4></div><div class="add-edit"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;&nbsp;<i class="fa fa-pencil-square"></i></div></div>

                  <div class="view-mode">
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Guitar Player </h5>
                           <p>with 5 years total experience.</p>
                        </div>
                     </div>
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Keyboard Player  </h5>
                           <p>with 3 years total experience and 1 year teaching experience.</p>
                        </div>
                     </div>
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Music Teacher  </h5>
                           <p>in lorem ipsum Music school.</p>
                        </div>
                     </div>
                  </div>
                  
                  <div class="edit-mode">
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Guitar Player </h5>
                           <p>with 5 years total experience.</p>
                        </div>
                     </div>
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Keyboard Player  </h5>
                           <p>with 3 years total experience and 1 year teaching experience.</p>
                        </div>
                     </div>
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Music Teacher  </h5>
                           <p>in lorem ipsum Music school.</p>
                        </div>
                     </div>
                  </div>

               </div>
               <div class="student-degree-right">
                  <h4>Education</h4>
                  <div class="student-degree-information">
                     <ul>
                        <li>Executive Program on Advanced Management</li>
                        <li>PHD in Dance</li>
                        <li>Bachelor of Engineering</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div> -->

         <!-- SKILL AND DGREE DIRECTLY FROM APP -->
         <div _ngcontent-c7="" class="student-degree-outer">
            <div _ngcontent-c7="" class="container">
               <div _ngcontent-c7="" class="student-details-left">

                  <div _ngcontent-c7="" class="heading"><div _ngcontent-c7=""><h4 _ngcontent-c7="">Skills &amp; Experience </h4></div><div _ngcontent-c7="" class="add-edit">
                  <i _ngcontent-c7="" class="fa fa-plus-circle" ng-reflect-ng-style="[object Object]" style="visibility: hidden;"></i>&nbsp;&nbsp;&nbsp;<i _ngcontent-c7="" class="fa fa-pencil-square"></i></div></div>

                  
                  <div _ngcontent-c7="" class="student-info-outer" ng-reflect-klass="student-info-outer" ng-reflect-ng-class="[object Object]">
                     <div _ngcontent-c7="" class="master-student-icon">
                        <img _ngcontent-c7="" src="../tn/assets/images/icon-student-details.png">
                     </div>
                     <div _ngcontent-c7="" class="master-student-deatils">
                        <div class="talent">AAA</div>
                        <div class="talent-exp">AAAAAAA</div>
                     </div>
                  </div>

                  <div _ngcontent-c7="" class="student-info-outer" ng-reflect-klass="student-info-outer" ng-reflect-ng-class="[object Object]">
                     <div _ngcontent-c7="" class="master-student-icon">
                        <img _ngcontent-c7="" src="../tn/assets/images/icon-student-details.png">
                     </div>
                     <div _ngcontent-c7="" class="master-student-deatils">
                        <h5>DDDD</h5>
                        <p>DDDD</p>
                        <div class="talent">DDDD</div>
                        <div class="talent-exp">DDDDDDDDDDD</div>
                        <div class="talent">&nbsp;</div>
                        <div class="talent-exp clear">DDDDDDDDDDD</div>
                        <div class="talent">&nbsp;</div>
                        <div class="talent-exp clear">DDDDDDDDDDD</div>
                     </div>
                  </div>

                  

                  <div _ngcontent-c7="" class="student-info-outer" style="display: none;" ng-reflect-klass="student-info-outer" ng-reflect-ng-class="[object Object]">
                    Share your skills.
                  </div>

                  <div _ngcontent-c7="" class="student-info-outer" style="display: block;" ng-reflect-klass="student-info-outer" ng-reflect-ng-class="[object Object]">
                    <form _ngcontent-c7="" novalidate="" ng-reflect-form="[object Object]" class="ng-untouched ng-pristine ng-valid">
                      <div _ngcontent-c7="" class="skill-container ng-untouched ng-pristine ng-valid" formarrayname="skills" ng-reflect-name="skills">
                        
                        <div _ngcontent-c7="" class="form-row">
                          <div _ngcontent-c7="" class="skill-container-input ng-untouched ng-pristine ng-valid" ng-reflect-name="0">
                            <input _ngcontent-c7="" class="skill ng-untouched ng-pristine ng-valid input" formcontrolname="talent" placeholder="Talent" ng-reflect-name="talent">
                            <input _ngcontent-c7="" class="exp ng-untouched ng-pristine ng-valid input" formcontrolname="exp" placeholder="Experience" ng-reflect-name="exp">
                          </div>
                          <div _ngcontent-c7="" class="skill-del"><i _ngcontent-c7="" class="fa fa-trash-o"></i></div>
                        </div>

                        <div _ngcontent-c7="">
                          <div _ngcontent-c7="" class="skill-container-input ng-untouched ng-pristine ng-valid" ng-reflect-name="0">
                            <input _ngcontent-c7="" class="skill ng-untouched ng-pristine ng-valid input" formcontrolname="talent" placeholder="Talent" ng-reflect-name="talent">
                            <input _ngcontent-c7="" class="exp ng-untouched ng-pristine ng-valid input" formcontrolname="exp" placeholder="Experience" ng-reflect-name="exp">
                          </div>
                          <div _ngcontent-c7="" class="skill-del"><i _ngcontent-c7="" class="fa fa-trash-o"></i></div>
                        </div>

                      </div>
                      
                        <div class="skills">
                            <p _ngcontent-c7="" class="form-err-msg" hidden="">Please enter valid talent value and experience as well.</p>
                        </div>

                        <div _ngcontent-c7="" class="tm-form-select-submitbuttons button-padding-right">
                            <button _ngcontent-c7="" type="submit" class="btn tm-form-skill-button submit">Submit</button> &nbsp; <button _ngcontent-c7="" type="button" class="btn tm-form-default-button cancel">Cancel</button>
                        </div>
                    </form>
                  </div>

               </div>



               <div _ngcontent-c7="" class="student-degree-right">
                  
                  <div _ngcontent-c7="" class="heading"><div _ngcontent-c7=""><h4 _ngcontent-c7="">Education </h4></div><div _ngcontent-c7="" class="add-edit"><i _ngcontent-c7="" class="fa fa-plus-circle" ng-reflect-ng-style="[object Object]" style="visibility: hidden;"></i>&nbsp;&nbsp;&nbsp;<i _ngcontent-c7="" class="fa fa-pencil-square"></i></div></div>

                  <div _ngcontent-c7="" class="student-degree-information" ng-reflect-klass="student-degree-information" ng-reflect-ng-class="[object Object]">
                     <ul _ngcontent-c7="">
                        <li _ngcontent-c7=""> wwww</li>

                        
                     </ul>
                  </div>
                  <div _ngcontent-c7="" class="student-degree-information" style="display: none;" ng-reflect-klass="student-degree-information" ng-reflect-ng-class="[object Object]">
                    Share your education or qualifications.
                  </div>

                  <div _ngcontent-c7="" class="student-degree-information" style="display: block;" ng-reflect-klass="student-degree-information" ng-reflect-ng-class="[object Object]">
                    <form _ngcontent-c7="" novalidate="" ng-reflect-form="[object Object]" class="ng-untouched ng-pristine ng-valid">
                      <div _ngcontent-c7="" class="education-container">
                        <div _ngcontent-c7="" formarrayname="educations" ng-reflect-name="educations" class="ng-untouched ng-pristine ng-valid">
                          <div _ngcontent-c7="">
                            <input _ngcontent-c7="" placeholder="education" ng-reflect-name="0" class="ng-untouched ng-pristine ng-valid">
                            <div _ngcontent-c7="" class="education-del" style="cursor: pointer;"><i _ngcontent-c7="" class="fa fa-trash-o"></i></div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="education">
                        <p class="form-err-msg">Please enter proper education information.</p>
                      </div>

                      <div class="tm-form-select-submitbuttons button-padding-right">
                        <button type="submit" class="btn tm-form-skill-button submit">Submit</button> &nbsp; <button type="button" class="btn tm-form-default-button cancel">Cancel</button>
                      </div>

                    </form>
                  </div>

               </div>
            </div>
        </div>
         <!-- SKILL AND DGREE DIRECTLY FROM APP -->

         <div class="about-mesection">
            <div class="container">
               <h4>About Me</h4>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.</p>
            </div>
         </div>
         <!-- <div class="fee-structure-outer">
            <div class="fee-structure">
               <div class="fee-structure-left">
                  <h4>Fee Structure</h4>
                  <div class="fees-deatisouter">
                     <div class="fees-deatis-left">
                        <h5 class="fees-subheading">Hourly Rate: </h5>
                        <div class="fees-prices-inner">
                           <span class="fees-discountprices">$45</span>
                           <span class="fees-cureentprices">$45</span>
                        </div>
                     </div>
                     <div class="fees-deatis-right">
                        <h5 class="fees-subheading">Group Discount:  </h5>

                        <div class="fees-groupcontent-outer">
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="fee-sechdule">
               <div class="fee-structure-left">
                  <h4>Schedule</h4>
                  <div class="fees-deatisouter">
                     <div class="fees-deatis-left">
                        <h5 class="fees-subheading">Everyday </h5>
                        <div class="everydayfees-prices-inner">
                           <span class="fees-every-day">11:30 AM to 12:00 PM</span>
                        </div>
                     </div>
                     <div class="fees-deatis-right">
                        <h5 class="fees-subheading">Week   </h5>
                        <div class="fees-groupcontent-outer">
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div> -->


         <!-- FEES DIRECTLY FROM APP -->
         <div _ngcontent-c9="" class="fee-structure-outer">
           <div _ngcontent-c9="" class="container">
             <div _ngcontent-c9="" class="fee-structure">
                <div _ngcontent-c9="" class="fee-structure-left">
                   
                   <div _ngcontent-c9="" class="heading"><div _ngcontent-c9=""><h4 _ngcontent-c9="">Lesson Details </h4></div><div _ngcontent-c9="" class="add-edit">
                   <i _ngcontent-c9="" class="fa fa-pencil-square"></i></div></div>

                   <div _ngcontent-c9="" class="fees-deatisouter" ng-reflect-klass="fees-deatisouter" ng-reflect-ng-class="[object Object]">
                       <div _ngcontent-c9="" class="fees-details-left">
                         
                         <div _ngcontent-c9="" class="fees-groupcontent-outer">
                           <div _ngcontent-c9="" class="fees-groupcontent-inner">
                             <span _ngcontent-c9="" class="fees-group-number">Recummended Lesson Dirationx:</span>
                             <span _ngcontent-c9="" class="fees-group-discount">12:20 Hours</span>
                           </div>
                           <div _ngcontent-c9="" class="fees-groupcontent-inner">
                             <span _ngcontent-c9="" class="fees-group-number">Max Number Of Student Per Lesson:</span>
                             <span _ngcontent-c9="" class="fees-group-discount">53</span>
                           </div>
                           <div _ngcontent-c9="" class="fees-groupcontent-inner">
                             <span _ngcontent-c9="" class="fees-group-number">Offer Free Trial Class</span>
                             <span _ngcontent-c9="" class="fees-group-discount">Yes</span>
                           </div>
                           <div _ngcontent-c9="" class="fees-groupcontent-inner">
                             <span _ngcontent-c9="" class="fees-group-number">Willingness to Travel</span>
                             <span _ngcontent-c9="" class="fees-group-discount">12 Miles</span>
                           </div>
                           <div _ngcontent-c9="" class="fees-groupcontent-inner">
                             <span _ngcontent-c9="" class="fees-group-number">Level Taught</span>
                             <span _ngcontent-c9="" class="fees-group-discount">Intermediate</span>
                           </div>
                         </div>
                       </div>
                   </div>

                   <div _ngcontent-c9="" class="fees-deatisouter" style="display: block;" ng-reflect-klass="fees-deatisouter" ng-reflect-ng-class="[object Object]">
                       <div _ngcontent-c9="" class="fees-details-left">
                         <form _ngcontent-c9="" novalidate="" ng-reflect-form="[object Object]" class="ng-untouched ng-pristine ng-valid">

                             <div _ngcontent-c9="" class="fees-groupcontent-form">
                                <span _ngcontent-c9="" class="fees-group-number">Recummended Lesson Dirationx:</span>
                                <span _ngcontent-c9="" class="fees-group-discount">
                                    <input _ngcontent-c9="" formcontrolname="lessonDuration" class="input" placeholder="HH:MM" type="time" ng-reflect-name="lessonDuration" class="ng-untouched ng-pristine ng-valid">
                                    <p _ngcontent-c9="" class="form-err-msg " hidden="">Please enter valid duration (number only).</p>
                                </span>
                             </div>
                             <div _ngcontent-c9="" class="fees-groupcontent-form">
                                <span _ngcontent-c9="" class="fees-group-number">Max Number Of Student Per Lesson:</span>
                                <span _ngcontent-c9="" class="fees-group-discount">
                                   <input _ngcontent-c9="" formcontrolname="studentPerLesson" class="input" placeholder="studentPerLesson" ng-reflect-name="studentPerLesson" class="ng-untouched ng-pristine ng-valid">
                                   <p _ngcontent-c9="" class="form-err-msg " hidden="">Please enter valid number of student (number only).</p>
                                </span>
                             </div>
                             <div _ngcontent-c9="" class="fees-groupcontent-form">
                               <span _ngcontent-c9="" class="fees-group-number">Offer Free Trial Class:</span>
                                <span _ngcontent-c9="" class="fees-group-discount">
                                   <select _ngcontent-c9="" formcontrolname="isFreeTrial" class="tm-form-select" ng-reflect-name="isFreeTrial" class="ng-untouched ng-pristine ng-valid">
                                     <option _ngcontent-c9="" value="No" ng-reflect-value="No">No</option>
                                     <option _ngcontent-c9="" value="Yes" ng-reflect-value="Yes">Yes</option>
                                   </select>
                                   <p _ngcontent-c9="" class="form-err-msg " hidden="">Please choose weather you provide free trial class.</p>
                                </span>
                             </div>
                             <div _ngcontent-c9="" class="fees-groupcontent-form">
                                <span _ngcontent-c9="" class="fees-group-number">Willingness to Travels:</span>
                                <span _ngcontent-c9="" class="fees-group-discount">
                                   <input _ngcontent-c9="" formcontrolname="travelradius" placeholder="Miles" class="input" ng-reflect-name="travelradius" class="ng-untouched ng-pristine ng-valid"> (miles)
                                   <p _ngcontent-c9="" class="form-err-msg " hidden="">Please enter valid radius (between 0 to 50).</p>
                                </span>
                             </div>
                             <div _ngcontent-c9="" class="fees-groupcontent-form">
                                <span _ngcontent-c9="" class="fees-group-number">Level Taught:</span>
                                <span _ngcontent-c9="" class="fees-group-discount">
                                   <select _ngcontent-c9="" formcontrolname="taughtLevel" class="tm-form-select" ng-reflect-name="taughtLevel" class="ng-untouched ng-pristine ng-valid">
                                   <option _ngcontent-c9="" value="0: Beginner" ng-reflect-ng-value="Beginner">Beginner</option><option _ngcontent-c9="" value="1: Intermediate" ng-reflect-ng-value="Intermediate">Intermediate</option><option _ngcontent-c9="" value="2: Advance" ng-reflect-ng-value="Advance">Advance</option>
                                   </select>
                                   <p _ngcontent-c9="" class="form-err-msg " hidden="">Please choose any level.</p>
                                </span>
                             </div>

                             <div class="tm-form-select-submitbuttons">
                               <button _ngcontent-c9="" type="submit" class="btn tm-form-default-button submit">Submit</button> &nbsp; <button _ngcontent-c9="" type="button" class="btn tm-form-default-button cancel">Cancel</button>
                             </div>

                         </form>
                       </div>
                   </div>

                </div>
             </div>

             <div _ngcontent-c9="" class="fee-sechdule">
                <div _ngcontent-c9="" class="fee-structure-right">
                     
                     <div _ngcontent-c9="" class="heading"><div _ngcontent-c9=""><h4 _ngcontent-c9="">Schedule </h4></div><div _ngcontent-c9="" class="add-edit"><i _ngcontent-c9="" class="fa fa-plus-circle" ng-reflect-ng-style="[object Object]" style="visibility: hidden;"></i>&nbsp;&nbsp;&nbsp;<i _ngcontent-c9="" class="fa fa-pencil-square"></i></div></div>

                     <div _ngcontent-c9="" class="fees-deatisouter" ng-reflect-klass="fees-deatisouter" ng-reflect-ng-class="[object Object]">
                         
                         <div _ngcontent-c9="" class="fees-deatis-right">
                             <h5 _ngcontent-c9="" class="fees-subheading">Weekly </h5>
                             <div _ngcontent-c9="" class="fees-groupcontent-outer">
                                 <div _ngcontent-c9="" class="fees-groupcontent-inner">
                                     <span _ngcontent-c9="" class="fees-group-number">Sunday: &nbsp;</span>
                                     <span _ngcontent-c9="" class="fees-group-discount"> 12 PM to 12 PM, 12 PM to 3 PM</span>
                                 </div>
                                 <div _ngcontent-c9="" class="fees-groupcontent-inner">
                                     <span _ngcontent-c9="" class="fees-group-number">Monday: &nbsp;</span>
                                     <span _ngcontent-c9="" class="fees-group-discount">12 PM to 3 PM</span>
                                 </div>
                                 <div _ngcontent-c9="" class="fees-groupcontent-inner">
                                     <span _ngcontent-c9="" class="fees-group-number">Tuesday: &nbsp;</span>
                                     <span _ngcontent-c9="" class="fees-group-discount"> 12 PM to 3 PM</span>
                                 </div>
                                 <div _ngcontent-c9="" class="fees-groupcontent-inner">
                                     <span _ngcontent-c9="" class="fees-group-number">Wednesday: &nbsp;</span>
                                     <span _ngcontent-c9="" class="fees-group-discount"> 12 PM to 3 PM</span>
                                 </div>
                                 <div _ngcontent-c9="" class="fees-groupcontent-inner">
                                     <span _ngcontent-c9="" class="fees-group-number">Thursday: &nbsp;</span>
                                     <span _ngcontent-c9="" class="fees-group-discount"> 12 PM to 3 PM</span>
                                 </div>
                                 <div _ngcontent-c9="" class="fees-groupcontent-inner">
                                     <span _ngcontent-c9="" class="fees-group-number">Friday: &nbsp;</span>
                                     <span _ngcontent-c9="" class="fees-group-discount"> 12 PM to 3 PM</span>
                                 </div>
                                 <div _ngcontent-c9="" class="fees-groupcontent-inner">
                                     <span _ngcontent-c9="" class="fees-group-number">Saturday: &nbsp;</span>
                                     <span _ngcontent-c9="" class="fees-group-discount"> 12 PM to 12 PM, 12 PM to 3 PM</span>
                                 </div>
                             </div>
                         </div>
                     </div>

                     <div _ngcontent-c9="" class="fees-deatisouter" style="display: block;" ng-reflect-klass="fees-deatisouter" ng-reflect-ng-class="[object Object]">
                       <form _ngcontent-c9="" novalidate="" ng-reflect-form="[object Object]" class="ng-untouched ng-pristine ng-valid">
                         <div _ngcontent-c9="" formarrayname="schedule" ng-reflect-name="schedule" class="ng-untouched ng-pristine ng-valid">
                            <div class="form-row">
                                <div class="day fees-subheading">Day</div>
                                <div class="start-time fees-subheading">Start Time</div>
                                <div class="end-time fees-subheading">End Time</div>
                                <div class="action fees-subheading"></div>
                            </div>

                           <div _ngcontent-c9="" class="form-row">
                                <div class="day">
                                   <select _ngcontent-c9="" class="tm-form-select" formcontrolname="weekday" ng-reflect-name="weekday">
                                     
                                     <option _ngcontent-c9="" value="0: Everyday" ng-reflect-ng-value="Everyday">Everyday</option><option _ngcontent-c9="" value="1: Weekend" ng-reflect-ng-value="Weekend">Weekend</option><option _ngcontent-c9="" value="2: Sunday" ng-reflect-ng-value="Sunday">Sunday</option><option _ngcontent-c9="" value="3: Monday" ng-reflect-ng-value="Monday">Monday</option><option _ngcontent-c9="" value="4: Tuesday" ng-reflect-ng-value="Tuesday">Tuesday</option><option _ngcontent-c9="" value="5: Wednesday" ng-reflect-ng-value="Wednesday">Wednesday</option><option _ngcontent-c9="" value="6: Thursday" ng-reflect-ng-value="Thursday">Thursday</option><option _ngcontent-c9="" value="7: Friday" ng-reflect-ng-value="Friday">Friday</option><option _ngcontent-c9="" value="8: Saturday" ng-reflect-ng-value="Saturday">Saturday</option>
                                   </select>
                                </div>
                                <div class="start-time">
                                    <input _ngcontent-c9="" formcontrolname="min" placeholder="Start" type="time" ng-reflect-name="min" class="input">
                                </div>
                                <div class="end-time">
                                    <input _ngcontent-c9="" formcontrolname="max" placeholder="End" type="time" ng-reflect-name="max" class="input">
                                </div>
                                <div class="action">
                                    <div class="delete" style="cursor: pointer;"><i _ngcontent-c7="" class="fa fa-trash-o"></i></div>
                                </div>
                             </div>
                           </div>
                         

                            <div _ngcontent-c9="" class="schedule">
                                <p _ngcontent-c9="" class="form-err-msg " hidden="">Please enter valid start/end time (24 format, ex: 14:30).</p>
                            </div>

                            <div class="tm-form-select-submitbuttons">
                                <button _ngcontent-c9="" type="submit" class="btn tm-form-default-button submit">Submit</button> &nbsp; <button _ngcontent-c9="" type="button" class="btn tm-form-default-button cancel">Cancel</button>
                            </div>

                          </div>
                       </form>
                     </div>

                </div>
             </div>
           </div>
         </div>
         <!-- DIRECTLY FROM APP -->

         <!-- NEW FEES STRUCTURE DIRECTLY FROM APP -->
         <div _ngcontent-c7="" class="fees-structure-outer">
            <div _ngcontent-c7="" class="container">
                <div _ngcontent-c7="" class="fees-structure">
                   
                      
                      <div _ngcontent-c7="" class="heading"><div _ngcontent-c7=""><h4 _ngcontent-c7="">Fee Structure </h4></div><div _ngcontent-c7="" class="add-edit"><i _ngcontent-c7="" class="fa fa-plus-circle" ng-reflect-ng-style="[object Object]" style="visibility: hidden;"></i>&nbsp;&nbsp;&nbsp;<i _ngcontent-c7="" class="fa fa-pencil-square"></i></div></div>

                      <div _ngcontent-c7="" class="fees-container" ng-reflect-klass="fees-deatisouter" ng-reflect-ng-class="[object Object]">

                        <div _ngcontent-c7="" class="top-fees-container">
                            <div _ngcontent-c7="" class="top-left-fees">
                              <div _ngcontent-c7="">Rate Per Hour</div>
                              <div _ngcontent-c7="">$50</div>

                              <div _ngcontent-c7="">Reason For Additional Fees</div>
                            </div>
                            <div _ngcontent-c7="" class="top-right-fees">
                              <div _ngcontent-c7="">Additional Fees Per Lesson</div>
                              <div _ngcontent-c7="">$2</div>
                            </div>
                        </div>

                        <div _ngcontent-c7=""> Fees message </div>
                        
                        <div _ngcontent-c7="" class="bottom-fees-container">
                          <table _ngcontent-c7="" class="desktop-grid fees-grid">
                            <tbody _ngcontent-c7=""><tr _ngcontent-c7="">
                              <th _ngcontent-c7="">No. of Student Enrolled</th>
                              <th _ngcontent-c7="">Rate per Lesson</th>
                              <th _ngcontent-c7="">Recommended Group Discount</th>
                              <th _ngcontent-c7="">Host Student Pays</th>
                              <th _ngcontent-c7="">non_host_student_pay</th>
                              <th _ngcontent-c7="">Estimated Earnings per lesson</th>
                              <th _ngcontent-c7="">Earn more with Group Lessons!</th>
                              <th _ngcontent-c7="">Action</th>
                            </tr>
                            <tr _ngcontent-c7="">
                              <td _ngcontent-c7=""></td>
                            </tr>
                          </tbody></table>
                        </div>

                      </div>
                      
                      <div _ngcontent-c7="" class="fees-container" style="display: block;" ng-reflect-klass="fees-deatisouter" ng-reflect-ng-class="[object Object]">
                        <div _ngcontent-c7="" class="">
                            <form _ngcontent-c7="" novalidate="" ng-reflect-form="[object Object]" class="ng-untouched ng-pristine ng-invalid">
                              <div _ngcontent-c7="" class=" feas-editor-area ng-untouched ng-pristine ng-invalid" formgroupname="fees" ng-reflect-name="fees">

                                <div _ngcontent-c7="" class="top-fees-container">
                                    <div _ngcontent-c7="" class="top-left-fees">

                                        <div _ngcontent-c7="">
                                          <div _ngcontent-c7="" class="left-lebel">Rate Per Hour</div>
                                          <div class="left-input">
                                            <input _ngcontent-c7="" formcontrolname="hourlyRate" placeholder="HH:MM" type="time" ng-reflect-name="hourlyRate" class="ng-untouched ng-pristine ng-valid">
                                            <p _ngcontent-c7="" class="form-err-msg " hidden="">Please enter valid amount for Hourly Rate.</p>
                                          </div>
                                        </div>

                                        <div _ngcontent-c7="">
                                          <div _ngcontent-c7="" class="left-lebel">Do you charge any additional fee?</div>
                                          <div class="left-input">
                                            <select _ngcontent-c7="" formcontrolname="extraFee" ng-reflect-name="extraFee" class="ng-untouched ng-pristine ng-valid">
                                                <option _ngcontent-c7="" value="Yes" ng-reflect-value="Yes">Yes</option>
                                                <option _ngcontent-c7="" value="No" ng-reflect-value="No">No</option>
                                            </select>
                                            <p _ngcontent-c7="" class="form-err-msg " hidden="">Please enter valid amount for Additional Fees.</p>
                                          </div>
                                        </div>
                                    </div>
                                    <div _ngcontent-c7="" class="top-right-fees">
                                        <div _ngcontent-c7="">
                                          <div _ngcontent-c7="" class="right-lebel">Additional Fees</div>
                                          <div class="right-input">
                                            <input _ngcontent-c7="" formcontrolname="extraFeeRate" placeholder="Additional Fees" ng-reflect-name="extraFeeRate" class="ng-untouched ng-pristine ng-valid">
                                            <p _ngcontent-c7="" class="form-err-msg " hidden="">Please enter valid amount for Additional Fees.</p>
                                          </div>
                                        </div>

                                        <div _ngcontent-c7="">
                                          <div _ngcontent-c7="" class="right-lebel">Reason for Additional Fees</div>
                                          <div class="right-input">
                                            <input _ngcontent-c7="" formcontrolname="extraFeeReason" placeholder="Additional Fees" ng-reflect-name="extraFeeReason" class="ng-untouched ng-pristine ng-valid">
                                            <p _ngcontent-c7="" class="form-err-msg " hidden="">Please enter valid amount for Additional Fees.</p>
                                          </div>
                                        </div>
                                    </div>
                                </div>

                                <div _ngcontent-c7="" class="bottom-fees-container">
                                  <table _ngcontent-c7="" class="desktop-grid7 ng-untouched ng-pristine ng-invalid" formarrayname="groupDiscount" ng-reflect-name="groupDiscount">
                                    
                                    <thead>
                                        <tr _ngcontent-c7="">
                                          <th _ngcontent-c7="">No. of Student Enrolled</th>
                                          <th _ngcontent-c7="">Rate per Lesson</th>
                                          <th _ngcontent-c7="">Recommended Group Discount</th>
                                          <th _ngcontent-c7="">Host Student Pays</th>
                                          <th _ngcontent-c7="">non_host_student_pay</th>
                                          <th _ngcontent-c7="">Estimated Earnings per lesson</th>
                                          <th _ngcontent-c7="">Earn more with Group Lessons!</th>
                                          <th _ngcontent-c7="">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr _ngcontent-c7="" class="col-md-12">
                                        
                                          <div _ngcontent-c7="" ng-reflect-name="0" class="ng-untouched ng-pristine ng-invalid">
                                            <td _ngcontent-c7=""> 
                                                <input _ngcontent-c7="" formcontrolname="students" placeholder="Student" type="text" ng-reflect-name="students" class="ng-untouched ng-pristine ng-invalid input">
                                            </td>
                                            <td _ngcontent-c7="">
                                                <input _ngcontent-c7="" formcontrolname="rate" placeholder="Rate" type="text" ng-reflect-name="rate" class="ng-untouched ng-pristine ng-invalid input">
                                            </td>

                                            <td _ngcontent-c7="">&nbsp;</td>
                                            <td _ngcontent-c7="">&nbsp;</td>
                                            <td _ngcontent-c7="">&nbsp;</td>
                                            <td _ngcontent-c7="">&nbsp;</td>
                                            <td _ngcontent-c7="">&nbsp;</td>

                                            <td _ngcontent-c7="" >
                                                <div class="delete"><i _ngcontent-c7="" class="fa fa-trash-o"></i></div>
                                            </td>
                                          </div>
                                        </tr>
                                        <!-- <tr _ngcontent-c7="" class="col-md-12">
                                        
                                          <div _ngcontent-c7="" ng-reflect-name="1" class="ng-untouched ng-pristine ng-invalid">
                                            <td _ngcontent-c7=""> 
                                                <input _ngcontent-c7="" formcontrolname="students" placeholder="Student" type="text" ng-reflect-name="students" class="ng-untouched ng-pristine ng-invalid">
                                            </td>
                                            <td _ngcontent-c7="">
                                                <input _ngcontent-c7="" formcontrolname="rate" placeholder="Rate" type="text" ng-reflect-name="rate" class="ng-untouched ng-pristine ng-invalid">
                                            </td>

                                            <td _ngcontent-c7="">&nbsp;</td>
                                            <td _ngcontent-c7="">&nbsp;</td>
                                            <td _ngcontent-c7="">&nbsp;</td>
                                            <td _ngcontent-c7="">&nbsp;</td>
                                            <td _ngcontent-c7="">&nbsp;</td>

                                            <td _ngcontent-c7="">
                                                <span _ngcontent-c7="" style="cursor: pointer;"> X </span>
                                            </td>
                                          </div>
                                        </tr> -->
                                    </tbody>
                                    <tfoot>
                                        <tr _ngcontent-c7="" class="error-row">
                                          <td>
                                              <div _ngcontent-c7="" class="col-md-12">
                                                  <p _ngcontent-c7="" class="form-err-msg " hidden="">Please enter valid student and rate.</p>
                                                </div>
                                          </td>
                                        </tr>
                                    </tfoot>
                                  </tbody></table>
                                </div>

                                <div _ngcontent-c7="" class="col-md-12" style="text-align: right;">
                                  <button _ngcontent-c7="" type="submit">Submit</button> &nbsp; <button _ngcontent-c7="" type="button">Cancel</button>
                                </div>

                              </div>
                            </form>
                        </div>
                      </div>
                      
                  
                </div>
            </div>
        </div>
        <!-- NEW FEES STRUCTURE DIRECTLY FROM APP -->



      </div>
      <div class="reviews-section">
         <div class="container">
            <h2>Reviews</h2>
            <img src="/tn/assets/images/skiils-titleicon.png">
            <div class="reviews-btmsec">
               <div class="reviewsbtm-totalsec">
                  <div class="reviews-totalrevie">Total Reviews <span>(15)</span></div>
                  <div class="list-view-select-outer">
                     <span>sort by</span>
                     <div class="available-sort-select list-view-select">
                        <select>
                           <option value="0">Rating</option>
                           <option value="1">2</option>
                           <option value="2">3</option>
                           <option value="3">4</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="dashbaord-post-inner">
                  <div class="dashbaord-post-details">
                     <div class="dashbaord-post-user">
                        <span class="post-userimg">
                        <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                        </span>
                        <span class="post-username">Lina Park</span>
                     </div>
                     <div class="dashbaord-post-time">
                        24 hours ago
                     </div>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <p class="post-msg">
                     Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                     <a href="">see more</a>
                  </p>
               </div>
               <div class="dashbaord-post-inner">
                  <div class="dashbaord-post-details">
                     <div class="dashbaord-post-user">
                        <span class="post-userimg">
                        <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                        </span>
                        <span class="post-username">Lina Park</span>
                     </div>
                     <div class="dashbaord-post-time">
                        24 hours ago
                     </div>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <p class="post-msg">
                     Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                     <a href="">see more</a>
                  </p>
               </div>
               <div class="dashbaord-post-inner">
                  <div class="dashbaord-post-details">
                     <div class="dashbaord-post-user">
                        <span class="post-userimg">
                        <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                        </span>
                        <span class="post-username">Lina Park</span>
                     </div>
                     <div class="dashbaord-post-time">
                        24 hours ago
                     </div>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <p class="post-msg">
                     Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                     <a href="">see more</a>
                  </p>
               </div>
               <a href="" class="seemore">see more</a>
            </div>
         </div>
      </div>
      <!-- <div class="photondvideos-section">
         <div class="container">
            <h2>My Policies</h2>
            <img src="/tn/assets/images/skiils-titleicon.png">
            <div class="videophoto-sec">
               <div class="phondvideo-videosection">
                  <img class="img-responsive" src="/tn/assets/images/video-img.jpg">
               </div>
               <div class="phondvideo-photosection">
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
               </div>
            </div>
         </div>
      </div> -->


         <!-- DIRECTLY FROM APP -->
         <div _ngcontent-c9="" class="photondvideos-section">
             <div _ngcontent-c9="" class="container">
                 <h2 _ngcontent-c9="">Photos and Videos</h2>
                 <img _ngcontent-c9="" src="assets/images/skiils-titleicon.png">
                 <div _ngcontent-c9="" class="add-edit"><i _ngcontent-c9="" class="fa fa-pencil-square"></i></div>

                 <div _ngcontent-c9="" class="videophoto-sec">
                    <div _ngcontent-c9="" class="media-form-container" style="display: block; text-align: left; margin-bottom: 10px;" ng-reflect-ng-class="[object Object]" class="show">

                        <div _ngcontent-c9="" class="top-row">
                          <label _ngcontent-c9=""><input _ngcontent-c9="" name="media_type" type="radio">Image</label>
                          <label _ngcontent-c9=""><input _ngcontent-c9="" name="media_type" type="radio">Video</label>
                          <div class="form-buttons">
                              <input type="submit" name="Save" value="Save" class="btn tm-form-default-button submit">
                              <input type="button" name="Cancel" value="Cancel" class="btn tm-form-default-button cancel">
                          </div>
                        </div>

                        <div _ngcontent-c9="" class="bottom-row">
                            <div _ngcontent-c9="">
                                <!-- <image-upload _ngcontent-c9="" _nghost-c8="" ng-reflect-max="100" ng-reflect-url="http://localhost:3001/api/user" ng-reflect-headers="[object Object]" ng-reflect-button-caption="Select Images!" ng-reflect-drop-box-message="Drop your images here!" ng-reflect-supported-extensions="jpg,png,jpeg"> -->
                                    <div _ngcontent-c8="" class="image-upload" filedrop="" ng-reflect-klass="image-upload" ng-reflect-ng-class="[object Object]" ng-reflect-accept="image/jpg,image/png,image/jpeg">
                                        <div _ngcontent-c8="" class="file-upload hr-inline-group">
                                          <label _ngcontent-c8="" class="upload button">
                                            <span _ngcontent-c8="">Select Images!</span>
                                            <input _ngcontent-c8="" multiple="" type="file" accept="image/jpg,image/png,image/jpeg">
                                          </label>
                                          <div _ngcontent-c8="" class="drag-box-message">Drop your images here!</div>
                                        </div>
                                        <div _ngcontent-c8="" class="image-container hr-inline-group">

                                        </div>
                                    </div>
                                <!-- </image-upload> -->
                            </div>
                            <div>
                                <input type="text" name="url" class="url-input">
                            </div>
                             
                         </div>
                    </div>

                     <div _ngcontent-c9="" class="phondvideo-videosection">
                         <img _ngcontent-c9="" class="img-responsive" src="http://localhost:3001/storage/59132720374ac0208cec29e4/media/3607-image2.jpg">
                     </div>

                     <div _ngcontent-c9="" class="phondvideo-photosection">
                       
                       <div _ngcontent-c9="" class="phondvideo-photobox showDelete" ng-reflect-klass="phondvideo-photobox" ng-reflect-ng-class="[object Object]">
                          <div _ngcontent-c9="" class="delete-media"><i _ngcontent-c9="" class="fa fa-trash-o"></i></div>
                          <img _ngcontent-c9="" class="img-responsive" src="http://localhost:3001/storage/59132720374ac0208cec29e4/media/3607-image2.jpg">
                          

                       </div>
                       <div _ngcontent-c9="" class="phondvideo-photobox showDelete" ng-reflect-klass="phondvideo-photobox" ng-reflect-ng-class="[object Object]">
                          <div _ngcontent-c9="" class="delete-media"><i _ngcontent-c9="" class="fa fa-trash-o"></i></div>
                          
                          <img _ngcontent-c9="" class="img-responsive" src="http://localhost:3001/storage/59132720374ac0208cec29e4/media/705893-image5.jpg">
                       </div>
                       <div _ngcontent-c9="" class="phondvideo-photobox showDelete" ng-reflect-klass="phondvideo-photobox" ng-reflect-ng-class="[object Object]">
                          <div _ngcontent-c9="" class="delete-media"><i _ngcontent-c9="" class="fa fa-trash-o"></i></div>
                          
                          <img _ngcontent-c9="" class="img-responsive" src="http://localhost:3001/storage/59132720374ac0208cec29e4/media/653298-image3.jpg">
                       </div>
                       <div _ngcontent-c9="" class="phondvideo-photobox showDelete" ng-reflect-klass="phondvideo-photobox" ng-reflect-ng-class="[object Object]">
                          <div _ngcontent-c9="" class="delete-media"><i _ngcontent-c9="" class="fa fa-trash-o"></i></div>
                          
                          <img _ngcontent-c9="" class="img-responsive" src="http://localhost:3001/storage/59132720374ac0208cec29e4/media/989593-image1.jpg">
                       </div>
                       <div _ngcontent-c9="" class="phondvideo-photobox showDelete" ng-reflect-klass="phondvideo-photobox" ng-reflect-ng-class="[object Object]">
                          <div _ngcontent-c9="" class="delete-media"><i _ngcontent-c9="" class="fa fa-trash-o"></i></div>
                          
                          <img _ngcontent-c9="" class="img-responsive" src="http://localhost:3001/storage/59132720374ac0208cec29e4/media/993327-avatar-1299805_960_720.png">

                       </div>
                       <div _ngcontent-c9="" class="phondvideo-photobox showDelete" ng-reflect-klass="phondvideo-photobox" ng-reflect-ng-class="[object Object]">
                          <div _ngcontent-c9="" class="delete-media"><i _ngcontent-c9="" class="fa fa-trash-o"></i></div>
                          
                          <iframe _ngcontent-c9="" allowfullscreen="" frameborder="0" height="100%" width="100%" src="https://www.youtube.com/embed/7uq-s0dMLsE"></iframe>
                          
                          <div _ngcontent-c9="" class="video-overlay"></div>
                       </div>

                       
                     </div>
                 </div>
             </div>
         </div>
         <!-- DIRECTLY FROM APP -->

      <div class="mypolicies-section">
         <div class="container">
            <h2>My Policies</h2>
            <img src="/tn/assets/images/skiils-titleicon.png">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
         </div>
      </div>
      <?php include('footer.php'); ?>
      <script src="/tn/assets/js/jquery-1.11.3.min.js" type="text/javascript"></script> 
      <script src="/tn/assets/js/bootstrap.min.js" type="text/javascript"></script> 
      <script type="text/javascript" src="/tn/assets/js/owl.carousel.js"></script> 
      <script src="/assets/js/enscroll-0.6.2.min.js"></script> 
   </body>
</html>

