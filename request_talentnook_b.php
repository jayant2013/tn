

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Talentnook</title>
      <base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      <link rel="stylesheet" href="/tn/assets/css/bootstrap.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/font-awesome.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/ui-screen.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/responsive-ui-screen.css" crossorigin="anonymous">
   </head>
   <body>
      <?php include('header.php'); ?>
      <div class="talent-master-outer header-botmsapce">
      	<div class="request-talentnook">
      		<div class="container">
	      		<div class="request-talentnook-bannercontent">
	      			<h2>Request A Talentnook</h2>
	      			<img class="img-responsive" src="../tn/assets/images/prof-4.png">
	      			<h3>Malorum BCC</h3>
	      		</div>
      		</div>
      	</div>

      	<div class="request-talentnook-feestruc">
      		<div class="container">
      			<div class="frquest-fee-leftsection">
      				<h4 class="fee-title">Fee Structure</h4>
      				<div class="fees-deatis-left request-talenamount">
                        <h5 class="fees-subheading">Hourly Rate: </h5>
                        <div class="fees-prices-inner">
                           <span class="fees-cureentprices">$45</span>
                        </div>
                     </div>

                     	<div class="fees-strutble">
		                    <table class="table">
							    <thead>
							      <tr>
							       	<th>No. of Student</th>
							        <th>Group Rate</th>
							        <th>Your Savings!</th>
							      </tr>
							    </thead>
							    <tbody>
							      <tr>
							       	<td>1</td>
							        <td>$65.00</td>
							        <td></td>
							      </tr>
							       <tr>
							       	<td>2-4</td>
							        <td>$55.00</td>
							        <td>15%</td>
							      </tr>
							       <tr>
							       	<td>5-7</td>
							        <td>$55.00</td>
							        <td>23%</td>
							      </tr>
							       <tr>
							       	<td>7-8</td>
							        <td>$55.00</td>
							        <td>46%</td>
							      </tr>
							    </tbody>
							</table>
							<a href="" class="fee-seemore">see more...</a>
						</div>
      			</div>
      			<div class="frquest-fee-rightsec">
	      				<div class="select-inner">
						    <label>Gender</label>
						    <select class="form-control">
							  <option>Talent 1</option>
							  <option>Talent 1</option>
							</select>
						</div>
						<div class="fee-input-inner">
						    <label>Class Schedule</label>
						    <input type="text" name="" placeholder="11:00 AM">
						</div>

						<div class="home-skills-checkbox">
			            	<input class="homecheckbox" name="cc1" id="m1" type="checkbox">
			            	<label for="m1"><span></span> I am willing to host the class </label>
			         	</div>
			         	<div class="host-lentnook">
			         		<a href="">Host the talentnook at your home and save up to <span>25 %</span> more on your lessons</a>
			         	</div>

      			</div>
      		</div>
      	</div>

      	<!-- student_enrollsec -->

      	<section class="student_enrollsec">
      		<div class="container">
      			<h3 class="student_title">Student to Enroll</h3>
      			<div class="student_title_check">
      				 <div class="home-skills-checkbox">
			            <input class="homecheckbox" name="cc1" id="s1" type="checkbox">
			            <label for="s1"><span></span> Student 1 </label>
			         </div>
			         <div class="home-skills-checkbox">
			            <input class="homecheckbox" name="cc1" id="s2" type="checkbox">
			            <label for="s2"><span></span> Student 1 </label>
			         </div>
			         <div class="home-skills-checkbox">
			            <input class="homecheckbox" name="cc1" id="s3" type="checkbox">
			            <label for="s3"><span></span> Student 1 </label>
			         </div>
			         <div class="home-skills-checkbox">
			            <input class="homecheckbox" name="cc1" id="s4" type="checkbox">
			            <label for="s4"><span></span> Student 1 </label>
			         </div>
      			</div>
      			<div class="add_student">
      				<a href="">
      					<i class="fa fa-plus-circle"></i>add student
      				</a>
      			</div>
      			
      		</div>
	      		<div class="add_student-formsec">
      				<div class="container">
	      				<form class="form-inline">
						  <div class="form-group">
						    <label>First Name</label>
						    <input type="text" class="form-control" placeholder="First Name">
						  </div>
						  <div class="form-group">
						    <label>Last Name</label>
						    <input type="text" class="form-control" placeholder="Last Name">
						  </div>
						   <div class="form-group">
						    <label>Gender</label>
						    <select class="form-control">
							  <option>Male</option>
							  <option>Female</option>
							</select>
						   </div>
						    <div class="form-group">
						    <label>Age</label>
						    <select class="form-control">
							  <option>18</option>
							  <option>18+</option>
							</select>
						   </div>
						    <div class="form-group">
						    <label>grade</label>
						    <select class="form-control">
							  <option>01</option>
							  <option>02</option>
							</select>
						   </div>
						   <div class="add-student-btnouter">
						    <label>a</label>
						  	<button type="submit" class="add-student-btn">Add Student</button>
						   </div>
						 
						</form>
					</div>	
      			</div>
      			<div class="cooment_sec">
      				<div class="container">
      					<textarea class="comments-msg" placeholder="Comments..."></textarea>
      					<div class="request-btns-inner">
      						<button class="request-btn">Request</button>
      						<button class="cancle-btn">Cancel</button>
      					</div>
      				</div>
      			</div>
      	</section> 

      	<!-- student_enrollsec -->
      
      </div>
      
      <?php include('footer.php'); ?>
      <script src="/tn/assets/js/jquery-1.11.3.min.js" type="text/javascript"></script> 
      <script src="/tn/assets/js/bootstrap.min.js" type="text/javascript"></script> 
      <script type="text/javascript" src="/tn/assets/js/owl.carousel.js"></script> 
      <script src="/assets/js/enscroll-0.6.2.min.js"></script> 
   </body>
</html>

