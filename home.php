

<!-- <section class="home-topslider header-botmsapce">
   <div class="homeslider-inner">
   	<span class="img-color-overlay"></span>
   	<img class="img-responsive" src="/tn/assets/images/slider-img01.jpg">
   </div>
   </section> -->
<section class="home-topslider header-botmsapce">
   <div class="main-slider full search-homeheader" id="search-homeheader">
   <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Wrapper for slides -->
      <div class="carousel-inner homeslider-inner">
        <div class="item active ">
            <div class="carousel-itemimg">
               <span class="img-color-overlay"></span>
               <img class="img-responsive" src="/tn/assets/images/slider-img01.jpg">
            </div>
            <div class="carousel-caption slide-caption">
               <div class="container relative">
                  <div class="cap-1">
                     <h3>Choose Activity</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non ultricies dui. Ut metus nunc, ullamcorper ultrices.</p>
                  </div>
                  <div class="cap-2">
                     <img class="img-responsive" src="/tn/assets/images/slide-icons.png" alt="" />
                  </div>
               </div>
            </div>
        </div>

        <div class="item">
            <div class="carousel-itemimg">
               <span class="img-color-overlay"></span>
               <img class="img-responsive" src="/tn/assets/images/slider-img01.jpg">
            </div>
            <div class="carousel-caption slide-caption">
               <div class="container relative">
                  <div class="cap-1">
                     <h3>Choose Activity</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non ultricies dui. Ut metus nunc, ullamcorper ultrices.</p>
                  </div>
                  <div class="cap-2">
                     <img class="img-responsive" src="/tn/assets/images/slide-icons.png" alt="" />
                  </div>
               </div>
            </div>
        </div>

        <div class="item">
            <div class="carousel-itemimg">
               <span class="img-color-overlay"></span>
               <img class="img-responsive" src="/tn/assets/images/slider-img01.jpg">
            </div>
            <div class="carousel-caption slide-caption">
               <div class="container relative">
                  <div class="cap-1">
                     <h3>Choose Activity</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non ultricies dui. Ut metus nunc, ullamcorper ultrices.</p>
                  </div>
                  <div class="cap-2">
                     <img class="img-responsive" src="/tn/assets/images/slide-icons.png" alt="" />
                  </div>
               </div>
            </div>
        </div>

        <div class="item">
            <div class="carousel-itemimg">
               <span class="img-color-overlay"></span>
               <img class="img-responsive" src="/tn/assets/images/slider-img01.jpg">
            </div>
            <div class="carousel-caption slide-caption">
               <div class="container relative">
                  <div class="cap-1">
                     <h3>Choose Activity</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non ultricies dui. Ut metus nunc, ullamcorper ultrices.</p>
                  </div>
                  <div class="cap-2">
                     <img class="img-responsive" src="/tn/assets/images/slide-icons.png" alt="" />
                  </div>
               </div>
            </div>
        </div>

            <!-- Indicators -->
            <ol class="carousel-indicators">
               <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
               <li data-target="#myCarousel" data-slide-to="1"></li>
               <li data-target="#myCarousel" data-slide-to="2"></li>
               <li data-target="#myCarousel" data-slide-to="3"></li>
               <li data-target="#myCarousel" data-slide-to="4"></li>
            </ol>
         </div>
      </div>
   </div>
</section>
<?php include('search.php'); ?>
<section class="how-it-works">
   <div class="container">
      <div class="how-it-works-title">
         <h2>How Will It Work?</h2>
         <img src="/tn/assets/images/home-howit-titleicon.png">
         <p>Our mantra is to create a network of parents and teachers that istrusted, convenient and where you get the value for your money.</p>
      </div>
      <div class="row">
         <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="how-it-works-box">
               <img src="/tn/assets/images/home-howit-trust.png">
               <h3>Trusted</h3>
               <p>TalentNooks will create a trusted learning network in your neighborhoods and connect you to local teachers.</p>
            </div>
         </div>
         <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="how-it-works-box">
               <img src="/tn/assets/images/home-howit-con.png">
               <h3>Convenient</h3>
               <p>TalentNooks will be held right in your or your neighbor’s home.</p>
            </div>
         </div>
         <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="how-it-works-box noborder">
               <img src="/tn/assets/images/home-howit-value.png">
               <h3>VALUE FOR MONEY</h3>
               <p>TalentNooks will be a free tool for you. Classes will be held in a group setting, facilitated by you or one of your neighbors thus bringing the costs down.</p>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="skill-ortelents">
   <div class="container">
      <h2>Do you have a skill or a talent that you can teach to kids?</h2>
      <img src="/tn/assets/images/skiils-titleicon.png">
      <div class="skill-ortelents-inputbtn-inner">
         <input type="email" name="" placeholder="Enter Your Email here">
         <button class="btn">Become a Talentmaster!</button>
      </div>
      <div class="skill-ortelents-botm">
         <p>We will never share your email address and you can opt out at any time.</p>
         <div class="home-skills-checkbox">
            <input class="homecheckbox" name="cc1" id="c27" type="checkbox">
            <label for="c27"><span></span> I can facilitate TalentNooks at my home. </label>
         </div>
      </div>
   </div>
</section>

