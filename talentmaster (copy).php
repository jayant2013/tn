<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Talentnook</title>
      <base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      <link rel="stylesheet" href="/tn/assets/css/bootstrap.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/font-awesome.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/ui-screen.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/responsive-ui-screen.css" crossorigin="anonymous">
   </head>
   <body>
      <?php include('header.php'); ?>
      <div class="talent-master-outer header-botmsapce">
         <div class="talent-master-topouter">
            <div class="talent-master-btnouter">
               <div class="container">
                  <button class="btn requestbtn">request a talentnook</button>
                  <button class="btn contact-btn">Contact Malorum</button>
               </div>
            </div>
            <div class="talentmasbanner-pro-middlesec-outer">
               <div class="container">
                      <div class="master-average-inner">
                        <i class="fa fa-clock-o"></i>  
                        Average Response Time <span>2hr24</span>
                     </div>
                  <div class="talentmasbanner-pro-middlesec">
                     <div class="map-rightpannal-imginner">
                        <div class="rightrating-img">
                           <i class="fa fa-star active"></i>
                           <i class="fa fa-star active"></i>
                           <i class="fa fa-star"></i>
                           <i class="fa fa-star"></i>
                           <i class="fa fa-star"></i>
                        </div>
                        <div class="middle-imginner">
                           <div class="rightpannal-img-left">
                              <img src="../tn/assets/images/right-pannal-img01.png">
                           </div>
                           <div class="rightpannal-middleimg"><img class="img-responsive" src="../tn/assets/images/prof-4.png"></div>
                           <div class="rightpannal-rightimg"><img src="../tn/assets/images/right-pannal-img02.png"></div>
                        </div>
                     </div>
                  </div>
                  <div class="master-banner-socail">
                        <ul>
                           <li>
                              <a href="">
                                 <i class="fa fa-facebook"></i>
                              </a>
                           </li>
                           <li>
                              <a href="">
                                 <i class="fa fa-twitter"></i>
                              </a>
                           </li>
                           <li>
                              <a href="">
                                 <i class="fa fa-linkedin"></i>
                              </a>
                           </li>
                           <li>
                              <a href="">
                                 <i class="fa fa-instagram"></i>
                              </a>
                           </li>
                           <li>
                              <a href="">
                                 <i class="fa fa-snapchat-ghost"></i>
                              </a>
                           </li>
                           <li>
                              <a href="">
                                 <i class="fa fa-yelp"></i>
                              </a>
                           </li>
                        </ul>
                     </div>
               </div>
            </div>
            <div class="talentmaster-banner-title-inner">
                  <div class="container">
                     <h2>Malorum BCC</h2>
                     <p>Lorem Ipsum is simply dummy text of the printing </p>
                     <p>
                        <i class="fa fa-map-marker"></i>
                        Richmond, KY, 40475 <span>New Yourk</span>
                     </p>
                  </div>   
            </div>
         </div>
         <div class="student-hoursly-rate-outer">
            <div class="container">
               <div class="student-hoursly-rate-left">
                  <div class="student-hour-inner">
                     <i class="fa fa-usd"></i>
                     Hourly Rate:
                     <span class="student-discoutprice">$45</span>
                     <span class="student-curremtprice">$40</span>
                  </div>
               </div>
               <div class="student-hoursly-rate-left">
                  <div class="student-hour-inner">
                     <i class="fa fa-map-signs"></i>Within Your Area Range
                  </div>
               </div>
            </div>
         </div>
         <div  class="student-degree-outer">
            <div class="container">
               <div class="student-details-left">

                  <div class="heading"><div><h4>Skills & Experience</h4></div><div class="add-edit"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;&nbsp;<i class="fa fa-pencil-square"></i></div></div>

                  <div class="view-mode">
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Guitar Player </h5>
                           <p>with 5 years total experience.</p>
                        </div>
                     </div>
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Keyboard Player  </h5>
                           <p>with 3 years total experience and 1 year teaching experience.</p>
                        </div>
                     </div>
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Music Teacher  </h5>
                           <p>in lorem ipsum Music school.</p>
                        </div>
                     </div>
                  </div>
                  
                  <div class="edit-mode">
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Guitar Player </h5>
                           <p>with 5 years total experience.</p>
                        </div>
                     </div>
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Keyboard Player  </h5>
                           <p>with 3 years total experience and 1 year teaching experience.</p>
                        </div>
                     </div>
                     <div class="student-info-outer">
                        <div class="master-student-icon">
                           <img src="tn/assets/images/icon-student-details.png">
                        </div>
                        <div class="master-student-deatils">
                           <h5>Music Teacher  </h5>
                           <p>in lorem ipsum Music school.</p>
                        </div>
                     </div>
                  </div>

               </div>
               <div class="student-degree-right">
                  <h4>Education</h4>
                  <div class="student-degree-information">
                     <ul>
                        <li>Executive Program on Advanced Management</li>
                        <li>PHD in Dance</li>
                        <li>Bachelor of Engineering</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="about-mesection">
            <div class="container">
               <h4>About Me</h4>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.</p>
            </div>
         </div>
         <div class="fee-structure-outer">
            <div class="fee-structure">
               <div class="fee-structure-left">
                  <h4>Fee Structure</h4>
                  <div class="fees-deatisouter">
                     <div class="fees-deatis-left">
                        <h5 class="fees-subheading">Hourly Rate: </h5>
                        <div class="fees-prices-inner">
                           <span class="fees-discountprices">$45</span>
                           <span class="fees-cureentprices">$45</span>
                        </div>
                     </div>
                     <div class="fees-deatis-right">
                        <h5 class="fees-subheading">Group Discount:  </h5>

                        <div class="fees-groupcontent-outer">
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">2 to 5</span>
                              <span class="fees-group-discount">10% discount</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="fee-sechdule">
               <div class="fee-structure-left">
                  <h4>Schedule</h4>
                  <div class="fees-deatisouter">
                     <div class="fees-deatis-left">
                        <h5 class="fees-subheading">Everyday </h5>
                        <div class="everydayfees-prices-inner">
                           <span class="fees-every-day">11:30 AM to 12:00 PM</span>
                        </div>
                     </div>
                     <div class="fees-deatis-right">
                        <h5 class="fees-subheading">Week   </h5>
                        <div class="fees-groupcontent-outer">
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                           <div class="fees-groupcontent-inner">
                              <span class="fees-group-number">Mon:</span>
                              <span class="fees-group-discount"> 10:30 AM to 12:00 PM, 1:00 PM to 2:30 PM</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="reviews-section">
         <div class="container">
            <h2>Reviews</h2>
            <img src="/tn/assets/images/skiils-titleicon.png">
            <div class="reviews-btmsec">
               <div class="reviewsbtm-totalsec">
                  <div class="reviews-totalrevie">Total Reviews <span>(15)</span></div>
                  <div class="list-view-select-outer">
                     <span>sort by</span>
                     <div class="available-sort-select list-view-select">
                        <select>
                           <option value="0">Rating</option>
                           <option value="1">2</option>
                           <option value="2">3</option>
                           <option value="3">4</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="dashbaord-post-inner">
                  <div class="dashbaord-post-details">
                     <div class="dashbaord-post-user">
                        <span class="post-userimg">
                        <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                        </span>
                        <span class="post-username">Lina Park</span>
                     </div>
                     <div class="dashbaord-post-time">
                        24 hours ago
                     </div>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <p class="post-msg">
                     Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                     <a href="">see more</a>
                  </p>
               </div>
               <div class="dashbaord-post-inner">
                  <div class="dashbaord-post-details">
                     <div class="dashbaord-post-user">
                        <span class="post-userimg">
                        <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                        </span>
                        <span class="post-username">Lina Park</span>
                     </div>
                     <div class="dashbaord-post-time">
                        24 hours ago
                     </div>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <p class="post-msg">
                     Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                     <a href="">see more</a>
                  </p>
               </div>
               <div class="dashbaord-post-inner">
                  <div class="dashbaord-post-details">
                     <div class="dashbaord-post-user">
                        <span class="post-userimg">
                        <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                        </span>
                        <span class="post-username">Lina Park</span>
                     </div>
                     <div class="dashbaord-post-time">
                        24 hours ago
                     </div>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <p class="post-msg">
                     Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                     <a href="">see more</a>
                  </p>
               </div>
               <a href="" class="seemore">see more</a>
            </div>
         </div>
      </div>
      <div class="photondvideos-section">
         <div class="container">
            <h2>My Policies</h2>
            <img src="/tn/assets/images/skiils-titleicon.png">
            <div class="videophoto-sec">
               <div class="phondvideo-videosection">
                  <img class="img-responsive" src="/tn/assets/images/video-img.jpg">
               </div>
               <div class="phondvideo-photosection">
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
                  <div class="phondvideo-photobox">
                     <img class="img-responsive" src="/tn/assets/images/photo-galeryimg.jpg">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="mypolicies-section">
         <div class="container">
            <h2>My Policies</h2>
            <img src="/tn/assets/images/skiils-titleicon.png">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
         </div>
      </div>
      <?php include('footer.php'); ?>
      <script src="/tn/assets/js/jquery-1.11.3.min.js" type="text/javascript"></script> 
      <script src="/tn/assets/js/bootstrap.min.js" type="text/javascript"></script> 
      <script type="text/javascript" src="/tn/assets/js/owl.carousel.js"></script> 
      <script src="/assets/js/enscroll-0.6.2.min.js"></script> 
   </body>
</html>

