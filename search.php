
<script type="text/javascript">
   function showHideSubMenu(){
      if($(".search-condition-dropdown").hasClass('show')){
         $(".search-condition-dropdown").removeClass('show');
         $(".search-condition-dropdown").addClass('hide');
      }else{
         $(".search-condition-dropdown").removeClass('hide');
         $(".search-condition-dropdown").addClass('show');
      }
   }
   function showHideAdvanceSearch(){
      if($(".home-available-sort-pannal").hasClass('show')){
         $(".home-available-sort-pannal").removeClass('show');
         $(".home-available-sort-pannal").addClass('hide');
      }else{
         $(".home-available-sort-pannal").removeClass('hide');
         $(".home-available-sort-pannal").addClass('show');
      }
   }
</script>
<section class="home-search-section">
   <div class="container">
      <div class="home-searchbar conditonal-search">

         <!-- CONDITIONAL MENU START -->
        <div class="conditonal-search-menu" onClick="showHideSubMenu()">
          <button>
            <span></span>
            <span></span>
            <span></span>
          </button>
        </div>

        <div class="leftpannal search-condition-dropdown hide">
          <ul>
            <li>
                <a href="javascript:void(0);">
                  <i class="fa fa-home"></i>Home
                </a>
            </li>
            <li>
               <a href="javascript:void(0);"> 
                 <img src="assets/images/left-pannal-logoicon.png">my_talentnooks
               </a>
            </li>
            <li>
                <a href="javascript:void(0);">
                  <i class="fa fa-comments"></i>talentnook_forum
                </a>
            </li>
            <div class="leftpannal-subchild">
                <ul>
                  <li>
                    <a href="javascript:void(0)">
                      <img src="assets/images/inbox-icon.png">inbox
                    </a>
                </li>
                </ul>
            </div>
          </ul>
        </div>
        <!-- CONDITIONAL MENU STOP -->


         <div class="home-searchbar-tabs">
            <ul>
               <li >
                  <a class="active" href="">Map View</a>
               </li>
               <li>
                  <a href="">List View</a>
               </li>
            </ul>
         </div>
         <div class="home-search-pannal">
            <div style="visibility: visible;" class="zipMsg">We read zipcode 342008 as your location. Please correct if it is incorrect </div>
            <input class="zipcode-input" type="" name="" placeholder="Zip Code">
            <input class="homesearch-input" type="" name="" placeholder="Search">
            <button class="home-srchbtn"><i class="fa fa-search"></i></button>
            <div class="form-err-msg" style="color: #f00;">Please enter valid zip code(eg. 07305)</div>
         </div>
         <div class="home-srchadvance">
            <a href="javascript:void(0)" onclick="showHideAdvanceSearch()">
            advance
            <i class="fa fa-plus"></i>
            <i class="fa fa-minus hidden"></i>	
            </a>
         </div>
      </div>
   </div>
   <div class="home-available-sort-pannal hide">
      <div class="container">
         <input type="text" class="address-input" name="" placeholder="Address">
         <div class="available-sort-select">
            <select>
               <option value="all">All Day</option>
               <option value="sunday">Sunday</option>
               <option value="monday">Monday</option>
               <option value="tuesday">Tuesday</option>
               <option value="wednesday">Wednesday</option>
               <option value="thursday">Thursday</option>
               <option value="friday">Friday</option>
               <option value="saturday">Saturday</option>
            </select>
         </div>
         <div class="available-sort-select">
            <select>
               <option value="0">Time</option>
               <option value="<11">Morning (before 11 AM)</option>
               <option value="11&13">Mid day (11 AM - 1 PM)</option>
               <option value="13&16">Afternoon (1 PM - 4 PM)</option>
               <option value=">16">Evening (After 4 PM)</option>
            </select>
         </div>
         <div class="advance-listview-chaeck">
            <input class="homecheckbox" name="cc1" id="c27" type="checkbox">
            <label for="c27"><span></span> Available Slot </label>
         </div>
      </div>
   </div>
   <div class="home-map-inner">
      <img class="img-responsive" src="/tn/assets/images/home-map.jpg">

      <div class="map-tooltip">
         <div class="map-tooltip-inner">
            <div class="map-imginner">
               <img class="img-responsive" src="../tn/assets/images/prof-4.png">
            </div>
            <div class="map-tooltip-content">
               <div class="map-tooltip-title">Finibus bonorum, <span>guitar </span></div>
               <div class="map-tooltip-rating">
                  <i class="fa fa-star active"></i>
                  <i class="fa fa-star active"></i>
                  <i class="fa fa-star active"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
               </div>
               <div class="map-tooltip-text">The guitar was my weapon The guitar was my weapon</div>
            </div>
            <div class="map-tooltip-arrow"></div>
         </div>
      </div>
      
      <div class="map-rightpannal">
         <div class="rightpannal-arrow">
            <i class="fa fa-caret-right"></i>
         </div>
         <div class="map-rightpannal-inner">
            <div class="map-rightpannal-inner-toppannal">
               <h2>Thomas Edward</h2>
               <p>Lorem Ipsum is simply dummy text of the printing </p>
               <div class="map-rightpannal-imginner">
                  <div class="rightrating-img">
                     <i class="fa fa-star active"></i>
                     <i class="fa fa-star active"></i>
                     <i class="fa fa-star"></i>
                     <i class="fa fa-star"></i>
                     <i class="fa fa-star"></i>
                  </div>
                  <div class="middle-imginner">
                     <div class="rightpannal-img-left">
                        <img src="../tn/assets/images/right-pannal-img01.png">
                     </div>
                     <div class="rightpannal-middleimg"><img class="img-responsive" src="../tn/assets/images/prof-4.png"></div>
                     <div class="rightpannal-rightimg"><img src="../tn/assets/images/right-pannal-img02.png"></div>
                  </div>
               </div>
               <div class="map-rightpannal-btmsec">
                  <div class="map-rightpannal-btmcontent">
                     <div class="rightpannal-details-first">
                        <span class="right-detilis-top">Free<sup>*</sup></span>
                        <span class="right-detilis-btm">$150</span>
                     </div>
                     <div class="rightpannal-details-secound">
                        <span class="right-detilis-top">2 slots open </span>
                        <span class="right-detilis-btm">Talentnook size 06</span>
                     </div>
                     <div class="rightpannal-details-third">
                        <span class="right-detilis-top">
                        US Marines. Jim W. Goggin. Jean. M. 6430
                        </span>
                        <span class="right-detilis-btm font-reg">Stream Side Court. Cummings.</span>
                     </div>
                  </div>
                  <div class="map-rightpannal-btm-schoudle">
                     <h4>Schedule</h4>
                     <div class="schedule-content">
                        <span class="schoudle-heading">Daily</span>
                        <span class="schoudle-text">9:00 AM to 11:00 AM</span>
                     </div>
                     <div class="schedule-content">
                        <span class="schoudle-heading">Weekly</span>
                        <span class="schoudle-text">Sun 10:30 AM to Sat 10:30AM</span>
                     </div>
                  </div>
                  <div class="map-rightpannal-btm-btns">
                     <button class="btn view-profile">View Profile</button>
                     <button class="btn interstedbtn">I am interested</button>
                     <h3>Talentmaster may offer lower fee based on class size.</h3>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="home-search-section">
   <div class="container">
      <div class="home-searchbar">
         <div class="home-searchbar-tabs">
            <ul>
               <li>
                  <a href="">Map View</a>
               </li>
               <li>
                  <a href="">List View</a>
               </li>
            </ul>
         </div>
         <div class="home-search-pannal">
            <input class="zipcode-input" type="" name="" placeholder="Zip Code">
            <input class="homesearch-input" type="" name="" placeholder="Search">
            <button class="home-srchbtn">
            <i class="fa fa-search"></i>
            </button>
         </div>
         <div class="home-srchadvance">
            <a href="">
            advance
            <i class="fa fa-plus"></i>
            <i class="fa fa-minus hidden"></i>	
            </a>
         </div>
      </div>
   </div>
   <div class="list-view-inner">
      <div class="container">
         <div class="list-view-topsection-inner">
            <div class="list-view-topsection">
               <div class="list-view-top-talentnook-tab">
                  <ul>
                     <li><a href="">Talentnooks</a></li>
                     <li><a href="">TalentMaster</a></li>
                  </ul>
               </div>
               <div class="list-view-select-outer">
                  <span>sort by</span>
                  <div class="available-sort-select list-view-select">
                     <select>
                        <option value="0">Rating</option>
                        <option value="1">2</option>
                        <option value="2">3</option>
                        <option value="3">4</option>
                     </select>
                  </div>
               </div>
            </div>
         </div>
        <div class="list-view-talentMaster">
            <div class="list-view-talentbox">
               <div class="list-view-talimginner">
                  <img class="img-responsive" src="../tn/assets/images/prof-1.jpg">
               </div>
               <div class="list-view-talentmastercontent">
                  <h2>TITLE NAME</h2>
                  <div class="list-view-talentmastercontent-rating">
                     <h3>James Lou</h3>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <span class="list-view-tag">
                  dance <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  getar <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  yoga <i class="fa fa-times"></i>
                  </span>
               </div>
            </div>
            <div class="list-view-talentbox">
               <div class="list-view-talimginner">
                  <img class="img-responsive" src="../tn/assets/images/prof-1.jpg">
               </div>
               <div class="list-view-talentmastercontent">
                  <h2>TITLE NAME</h2>
                  <div class="list-view-talentmastercontent-rating">
                     <h3>James Lou</h3>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <span class="list-view-tag">
                  dance <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  getar <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  yoga <i class="fa fa-times"></i>
                  </span>
               </div>
            </div>
            <div class="list-view-talentbox">
               <div class="list-view-talimginner">
                  <img class="img-responsive" src="../tn/assets/images/prof-1.jpg">
               </div>
               <div class="list-view-talentmastercontent">
                  <h2>TITLE NAME</h2>
                  <div class="list-view-talentmastercontent-rating">
                     <h3>James Lou</h3>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <span class="list-view-tag">
                  dance <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  getar <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  yoga <i class="fa fa-times"></i>
                  </span>
               </div>
            </div>
            <div class="list-view-talentbox">
               <div class="list-view-talimginner">
                  <img class="img-responsive" src="../tn/assets/images/prof-1.jpg">
               </div>
               <div class="list-view-talentmastercontent">
                  <h2>TITLE NAME</h2>
                  <div class="list-view-talentmastercontent-rating">
                     <h3>James Lou</h3>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <span class="list-view-tag">
                  dance <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  getar <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  yoga <i class="fa fa-times"></i>
                  </span>
               </div>
            </div>
            <div class="list-view-talentbox">
               <div class="list-view-talimginner">
                  <img class="img-responsive" src="../tn/assets/images/prof-1.jpg">
               </div>
               <div class="list-view-talentmastercontent">
                  <h2>TITLE NAME</h2>
                  <div class="list-view-talentmastercontent-rating">
                     <h3>James Lou</h3>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <span class="list-view-tag">
                  dance <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  getar <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  yoga <i class="fa fa-times"></i>
                  </span>
               </div>
            </div>
            <div class="list-view-talentbox">
               <div class="list-view-talimginner">
                  <img class="img-responsive" src="../tn/assets/images/prof-1.jpg">
               </div>
               <div class="list-view-talentmastercontent">
                  <h2>TITLE NAME</h2>
                  <div class="list-view-talentmastercontent-rating">
                     <h3>James Lou</h3>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <span class="list-view-tag">
                  dance <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  getar <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  yoga <i class="fa fa-times"></i>
                  </span>
               </div>
            </div>
            <div class="list-view-talentbox">
               <div class="list-view-talimginner">
                  <img class="img-responsive" src="../tn/assets/images/prof-1.jpg">
               </div>
               <div class="list-view-talentmastercontent">
                  <h2>TITLE NAME</h2>
                  <div class="list-view-talentmastercontent-rating">
                     <h3>James Lou</h3>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <span class="list-view-tag">
                  dance <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  getar <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  yoga <i class="fa fa-times"></i>
                  </span>
               </div>
            </div>
            <div class="list-view-talentbox">
               <div class="list-view-talimginner">
                  <img class="img-responsive" src="../tn/assets/images/prof-1.jpg">
               </div>
               <div class="list-view-talentmastercontent">
                  <h2>TITLE NAME</h2>
                  <div class="list-view-talentmastercontent-rating">
                     <h3>James Lou</h3>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <span class="list-view-tag">
                  dance <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  getar <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  yoga <i class="fa fa-times"></i>
                  </span>
               </div>
            </div>
            <div class="list-view-talentbox">
               <div class="list-view-talimginner">
                  <img class="img-responsive" src="../tn/assets/images/prof-1.jpg">
               </div>
               <div class="list-view-talentmastercontent">
                  <h2>TITLE NAME</h2>
                  <div class="list-view-talentmastercontent-rating">
                     <h3>James Lou</h3>
                     <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  </div>
                  <span class="list-view-tag">
                  dance <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  getar <i class="fa fa-times"></i>
                  </span>
                  <span class="list-view-tag">
                  yoga <i class="fa fa-times"></i>
                  </span>
               </div>
            </div>
         </div>
         <div class="map-rightpannal list-view-right-sec">
            <div class="map-rightpannal-inner">
               <div class="map-rightpannal-inner-toppannal">
                  <h2>Thomas Edward</h2>
                  <p>Lorem Ipsum is simply dummy text of the printing </p>
                  <div class="map-rightpannal-imginner">
                     <div class="rightrating-img list-view">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                     <div class="middle-imginner">
                        <div class="rightpannal-img-left">
                           <img src="../tn/assets/images/right-pannal-img01.png">
                        </div>
                        <div class="rightpannal-middleimg"><img class="img-responsive" src="../tn/assets/images/prof-4.png"></div>
                        <div class="rightpannal-rightimg"><img src="../tn/assets/images/right-pannal-img02.png"></div>
                     </div>
                  </div>
                  <div class="map-rightpannal-btmsec">
                     <div class="map-rightpannal-btmcontent">
                        <div class="rightpannal-details-first">
                           <span class="right-detilis-top">Free<sup>*</sup></span>
                           <span class="right-detilis-btm">$150</span>
                        </div>
                        <div class="rightpannal-details-secound">
                           <span class="right-detilis-top">Last Active On </span>
                           <span class="right-detilis-btm">21-01-2017</span>
                        </div>
                        <div class="rightpannal-details-third">
                           <span class="right-detilis-top">
                           US Marines. Jim W. Goggin. Jean. M. 6430
                           </span>
                           <span class="right-detilis-btm font-reg">Stream Side Court. Cummings.</span>
                        </div>
                     </div>
                     <div class="map-rightpannal-btm-schoudle">
                        <h4>Schedule</h4>
                        <div class="schedule-content">
                           <span class="schoudle-heading">Daily</span>
                           <span class="schoudle-text">9:00 AM to 11:00 AM</span>
                        </div>
                     </div>
                     <div class="list-view-rgt-about">
                        <h5>About Me:</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non ultricies dui. Ut metus nunc, ullamcorper ultrices.</p>
                        <ul>
                           <li>Guitar Player with 5 years total experience.</li>
                           <li>Keyboard Player with 3 years total experience and 1 year teaching experience.</li>
                        </ul>
                     </div>
                     <div class="map-rightpannal-btm-btns">
                        <button class="btn view-profile">View Profile</button>
                        <button class="btn interstedbtn">I am interested</button>
                        <h3>Talentmaster may offer lower fee based on class size.</h3>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

