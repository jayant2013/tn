

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Talentnook</title>
      <base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      <link rel="stylesheet" href="/tn/assets/css/bootstrap.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/font-awesome.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/ui-screen.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/responsive-ui-screen.css" crossorigin="anonymous">
   </head>
   <body>
      <?php include('header.php'); ?>
      <section class="member-profile-outer header-botmsapce">
         <div class="container">
            <div class="row">
               <div class="col-xs-12 col-sm-3 col-md-9 mem-prof-pd-r0">
                  <div class="mem-prof-topouter">
                     <div class="mem-prof-leftouter">
                        <div class="mem-prof-bgouter">
                           <div class="mem-prof-inner">
                              <img class="img-responsive" src="../tn/assets/images/prof-4.png">
                           </div>
                           <div class="mem-prof-title">Malorum EDward</div>
                        </div>
                        <div class="mem-prof-leftsocial">
                           <div class="foot-btm-socail">
                              <a href=""><img src="/tn/assets/images/facebook.png"></a>
                              <a href=""><img src="/tn/assets/images/twitter-footer.png"></a>
                              <a href=""><img src="/tn/assets/images/instagram-footer.png"></a>
                              <a href=""><img src="/tn/assets/images/twits.png"></a>
                           </div>
                        </div>
                     </div>
                     <div class="mem-prof-rightouter">
                        <a href="" class="linkmember">Link to member agreement</a>
                        <div class="mem-prof-address">
                           <div class="mem-prof-address-inner">
                              <span class="addresicon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                              2371 Danby Circle, San Ramon, CA 94583
                              <span class="add-editicon">
                              <a href=""><i class="fa fa-pencil-square-o"></i></a>
                              </span>
                           </div>
                           <div class="mem-prof-address-inner">
                              <span class="addresicon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                              Neighborhood  where I live: 
                              <span class="add-links"><a href="">Summer Glen</a></span>
                           </div>
                           <div class="mem-prof-address-inner">
                              <span class="addresicon">
                              <i class="fa fa-envelope"></i>
                              </span>
                              Preferred method of contact: 
                              <span class="add-links"><a href="">Email</a></span>
                           </div>
                        </div>
                        <div class="mem-prof-phonenumouter">
                           <div class="mem-prof-phoinner">
                              <span class="mem-pro-photitle">Phone number: </span>
                              <span  class="mem-pro-phonum">(415) 936-9944</span>
                           </div>
                           <div class="mem-prof-phoinner">
                              <span class="mem-pro-photitle">Alternate phone number:  </span>
                              <span  class="mem-pro-phonum">(415) 258-9943</span>
                           </div>
                           <div class="mem-prof-phoinner">
                              <span class="mem-pro-photitle">Email address: </span>
                              <span  class="mem-pro-phonum">malorum.b@yahoo.com</span>
                           </div>
                        </div>
                        <div class="mem-pro-talentprof">
                           <div class="mem-pro-talentprof-left">
                              <span class="talent-int">Talents of Interest</span>
                              <span  class="talent-guitar">Guitar, Maths, Salsa</span>
                           </div>
                           <div class="mem-pro-talentprof-right">
                              <span class="talent-int">I can host Talentnook at my home.</span>
                              <button class="btn yesbtn">yes</button>
                           </div>
                        </div>
                        <div class="setuppaymen-bg">
                           <button class="btn setuppaymnt-btn">Setup payment account</button>
                        </div>
                     </div>
                     <div class="member-student-table">
                     <div class="member-studenttitel">Students
                        <span>
                        <a href=""><i class="fa fa-pencil-square-o"></i></a>
                        </span>
                     </div>
                     <div class="student-table-menb">
                        <div class="table-responsive">
                           <table class="table table-hover">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>Gender</th>
                                    <th>Grade</th>
                                    <th>School</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       <span class="post-userimg">
                                       <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                                       </span>
                                    </td>
                                    <td>Lina Park</td>
                                    <td>15</td>
                                    <td>M</td>
                                    <td>2</td>
                                    <td>Mercer Island high school</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <span class="post-userimg">
                                       <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                                       </span>
                                    </td>
                                    <td>Lina Park</td>
                                    <td>15</td>
                                    <td>M</td>
                                    <td>2</td>
                                    <td>Mercer Island high school</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <span class="post-userimg">
                                       <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                                       </span>
                                    </td>
                                    <td>Lina Park</td>
                                    <td>15</td>
                                    <td>M</td>
                                    <td>2</td>
                                    <td>Mercer Island high school</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <span class="post-userimg">
                                       <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                                       </span>
                                    </td>
                                    <td>Lina Park</td>
                                    <td>15</td>
                                    <td>M</td>
                                    <td>2</td>
                                    <td>Mercer Island high school</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                     <div class="member-feedbackcontent">
                        <div class="member-studenttitel">Feedback provided</div>
                        <div class="dashbaord-post-inner">
                           <div class="feedback-provided-left">
                              <span class="post-userimg">
                              <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                              </span>
                           </div>
                           <div class="feedback-provided-right">
                              <div class="dashbaord-post-details">
                                 <div class="dashbaord-post-user">
                                    <span class="post-username">Lina Park</span>
                                 </div>
                                 <div class="dashbaord-post-time">
                                    24 hours ago
                                 </div>
                              </div>
                              <div class="map-tooltip-rating">
                                 <i class="fa fa-star active"></i>
                                 <i class="fa fa-star active"></i>
                                 <i class="fa fa-star active"></i>
                                 <i class="fa fa-star"></i>
                                 <i class="fa fa-star"></i>
                              </div>
                              <p class="post-msg">
                                 Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                                 <a href="">see more</a>
                              </p>
                           </div>
                        </div>
                        <div class="dashbaord-post-inner">
                           <div class="feedback-provided-left">
                              <span class="post-userimg">
                              <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                              </span>
                           </div>
                           <div class="feedback-provided-right">
                              <div class="dashbaord-post-details">
                                 <div class="dashbaord-post-user">
                                    <span class="post-username">Lina Park</span>
                                 </div>
                                 <div class="dashbaord-post-time">
                                    24 hours ago
                                 </div>
                              </div>
                              <div class="map-tooltip-rating">
                                 <i class="fa fa-star active"></i>
                                 <i class="fa fa-star active"></i>
                                 <i class="fa fa-star active"></i>
                                 <i class="fa fa-star"></i>
                                 <i class="fa fa-star"></i>
                              </div>
                              <p class="post-msg">
                                 Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                                 <a href="">see more</a>
                              </p>
                           </div>
                        </div>
                        <div class="dashbaord-post-inner">
                           <div class="feedback-provided-left">
                              <span class="post-userimg">
                              <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                              </span>
                           </div>
                           <div class="feedback-provided-right">
                              <div class="dashbaord-post-details">
                                 <div class="dashbaord-post-user">
                                    <span class="post-username">Lina Park</span>
                                 </div>
                                 <div class="dashbaord-post-time">
                                    24 hours ago
                                 </div>
                              </div>
                              <div class="map-tooltip-rating">
                                 <i class="fa fa-star active"></i>
                                 <i class="fa fa-star active"></i>
                                 <i class="fa fa-star active"></i>
                                 <i class="fa fa-star"></i>
                                 <i class="fa fa-star"></i>
                              </div>
                              <p class="post-msg">
                                 Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                                 <a href="">see more</a>
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  </div>
                  
               </div>
               <div class="col-xs-12 col-sm-9 col-md-3 member-rightsec">
                  <div class="member-rightsectinon">
                     <div class="mem-mydasborouter">
                     <button class="btn mydashbord-btn">
                        <span><i class="fa fa-long-arrow-left"></i></span>
                        My Dashboard
                     </button>
                     </div>
                     <div class="member-right-budegs">
                        <ul>
                           <li>
                              <a href="#">Member since <span> July 12, 2017</span></a>
                           </li>
                           <li>
                              <a href="#">Talentnooks hosted <span class="badge">4</span></a>
                           </li>
                           <li>
                              <a href="#">Talentnooks enrolled in <span class="badge">5</span></a>
                           </li>
                            <li>
                              <a href="#">Lessons to date <span class="badge">26</span></a>
                           </li>
                        </ul>
                     </div>
                     <div class="btn-chnage-btnouter">
                        <a href="" class="btn chnage-btn">Change Password</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('footer.php'); ?>
      <script src="/tn/assets/js/jquery-1.11.3.min.js" type="text/javascript"></script> 
      <script src="/tn/assets/js/bootstrap.min.js" type="text/javascript"></script> 
      <script type="text/javascript" src="/tn/assets/js/owl.carousel.js"></script> 
      <script src="/assets/js/enscroll-0.6.2.min.js"></script> 
   </body>
</html>

