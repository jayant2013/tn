<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Talentnook</title>
      <base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      <link rel="stylesheet" href="/tn/assets/css/bootstrap.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/font-awesome.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/ui-screen.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/responsive-ui-screen.css" crossorigin="anonymous">
   </head>
   <body>
      <?php include('header.php'); ?>
	      <section class="middle-title-banner header-botmsapce">
	         <div class="container">
	            <h4>Dashbaord</h4>
	         </div>
	      </section>
	      <section class="dashbaord-content-section">
	         <div class="container">
	            <div class="row">
	               <div class="col-xs-12 col-sm-12 col-md-12">
	                  <div class="leftpannal">
	                     <ul>
	                        <li>
	                           <a href="">
	                           <i class="fa fa-home"></i>
	                           Home
	                           </a>
	                        </li>
	                        <li>
	                           <a href=""> 
	                           <img src="/tn/assets/images/left-pannal-logoicon.png">
	                           My Talentnooks
	                           </a>
	                        </li>
	                        <li>
	                           <a href="">
	                           <i class="fa fa-comments"></i>
	                           Talentnook Forum</a>
	                        </li>
	                       <div class="leftpannal-subchild">
	                          <ul>
	                              <li>
	                           <a href="">
	                           <img src="/tn/assets/images/inbox-icon.png">
	                           inbox</a>
	                        </li>
	                          </ul>
	                       </div>
	                     </ul>
	                  </div>
	                  <div class="dashbaord-list-view-rightsection">
	                    <div class="dashbaord-list-view-rightinner">

							  <!-- Nav tabs -->
							  <ul class="nav nav-tabs" role="tablist">
							    <li role="presentation" class="active"><a href="#mapview" aria-controls="home" role="tab" data-toggle="tab">Map View</a></li>
							    <li role="presentation"><a href="#vist-view" aria-controls="profile" role="tab" data-toggle="tab">List View</a></li>
							    <div class="list-view-top-right">
								    <div class="list-viewShowInactive">
							            <input class="homecheckbox" name="cc1" id="c27" type="checkbox">
							            <label for="c27"><span></span> Show Inactive</label>
							        </div>
							        <a href="" class="btn launch-newtalnok">Launch New Talentnook</a>
						        </div>
							  </ul>

							  <!-- Tab panes -->
							  <div class="tab-content">
							    <div role="tabpanel" class="tab-pane active" id="mapview">
							    	<div class="mapview-pannal">Map Here</div>
							    </div>
							    <div role="tabpanel" class="tab-pane" id="vist-view">
							    	<div class="dashbaord-list-view-content">
							    		<div class="dashbaord-list-view-pannal">
							    			<div class="list-view-con-header">New Request</div>
								    		<div class="dashbaord-list-view-sub-outer">
								    			<div class="dashbaord-list-view-subpannal">
								    				<div class="list-view-top">
								    					<div class="list-view-name">Jack Doe</div>
								    					<div class="list-view-same list-view-miles">
								    						<span><i class="fa fa-bookmark"></i></span>
								    						9.2 Miles away in Dublin
								    					</div>
								    					<div class="list-view-same view-student">
								    						student <span>5</span>
								    					</div>
								    					<div class="list-view-same view-host">
								    						student <span><i class="fa fa-check"></i></span>
								    					</div>
								    					<div class="list-view-same view-host">
								    						<span><i class="fa fa-calendar"></i></span>
								    						27-Jul-17  
								    					</div>
								    					<div class="list-view-same view--message">
								    						<span><i class="fa fa-envelope"></i></span>
								    						<span class="view-msgnotifi">4</span>
								    					</div>
								    					<div class="list-view-new">New</div>

								    				</div>
								    				<div class="list-view-bootm">
								    					<div class="list-view-math">
								    						<span><i class="fa fa-star"></i></span>
								    						Maths
								    					</div>
								    					<div class="list-view-same btm-waitng">
								    						<span><i class="fa fa-file-text"></i></span>
								    						Waiting on confirmation of start date from Jill 
								    					</div>
								    				</div>
								    			</div>
								    			<div class="dashbaord-list-view-subpannal">
								    				<div class="list-view-top">
								    					<div class="list-view-name">Jack Doe</div>
								    					<div class="list-view-same list-view-miles">
								    						<span><i class="fa fa-bookmark"></i></span>
								    						9.2 Miles away in Dublin
								    					</div>
								    					<div class="list-view-same view-student">
								    						student <span>5</span>
								    					</div>
								    					<div class="list-view-same view-host">
								    						student <span><i class="fa fa-check"></i></span>
								    					</div>
								    					<div class="list-view-same view-host">
								    						<span><i class="fa fa-calendar"></i></span>
								    						27-Jul-17  
								    					</div>
								    					<div class="list-view-same view--message">
								    						<span><i class="fa fa-envelope"></i></span>
								    						<span class="view-msgnotifi">4</span>
								    					</div>
								    					<div class="list-view-new">New</div>

								    				</div>
								    				<div class="list-view-bootm">
								    					<div class="list-view-math">
								    						<span><i class="fa fa-star"></i></span>
								    						Maths
								    					</div>
								    					<div class="list-view-same btm-waitng">
								    						<span><i class="fa fa-file-text"></i></span>
								    						Waiting on confirmation of start date from Jill 
								    					</div>
								    				</div>
								    			</div>
								    			<div class="dashbaord-list-view-subpannal">
								    				<div class="list-view-top">
								    					<div class="list-view-name">Jack Doe</div>
								    					<div class="list-view-same list-view-miles">
								    						<span><i class="fa fa-bookmark"></i></span>
								    						9.2 Miles away in Dublin
								    					</div>
								    					<div class="list-view-same view-student">
								    						student <span>5</span>
								    					</div>
								    					<div class="list-view-same view-host">
								    						student <span><i class="fa fa-check"></i></span>
								    					</div>
								    					<div class="list-view-same view-host">
								    						<span><i class="fa fa-calendar"></i></span>
								    						27-Jul-17  
								    					</div>
								    					<div class="list-view-same view--message">
								    						<span><i class="fa fa-envelope"></i></span>
								    						<span class="view-msgnotifi">4</span>
								    					</div>
								    					<div class="list-view-new">New</div>

								    				</div>
								    				<div class="list-view-bootm">
								    					<div class="list-view-math">
								    						<span><i class="fa fa-star"></i></span>
								    						Maths
								    					</div>
								    					<div class="list-view-same btm-waitng">
								    						<span><i class="fa fa-file-text"></i></span>
								    						Waiting on confirmation of start date from Jill 
								    					</div>
								    				</div>
								    			</div>
								    			
								    		</div>	
							    		</div>

							    		<div class="dashbaord-list-view-pannal">
							    			<div class="list-view-con-header">Ongoing</div>
								    		<div class="dashbaord-list-view-sub-outer">
								    			<div class="dashbaord-list-view-subpannal">
								    				<div class="list-view-top">
								    					<div class="list-view-name">Eric Jones </div>
								    					<div class="list-view-same list-view-miles">
								    						<span><i class="fa fa-map-marker"></i></span>
								    						2241, Tawny Terr, Dublin
								    					</div>
								    					<div class="list-view-same view-student">
								    						Enrolled:  <span>4 out 6</span>
								    					</div>

								    					<div class="list-view-same view-student">
								    						New Requests:  <span>1</span>
								    					</div>

								    					<div class="list-view-same view-student">
								    						Waitlisted:   <span>0</span>
								    					</div>


								    				</div>
								    				<div class="list-view-bootm">
								    					<div class="list-view-math">
								    						<span><i class="fa fa-star"></i></span>
								    						Maths
								    					</div>
								    					<div class="list-view-same view-host">
								    						<span><i class="fa fa-calendar"></i></span>
								    						Next 16-May-17 11:00 AM
								    					</div>
								    					<div class="list-view-same view-host">
								    						<span><i class="fa fa-calendar"></i></span>
								    						Ends 15-Dec-17
								    					</div>
								    					<div class="list-view-same btm-waitng">
								    						<span><i class="fa fa-file-text"></i></span>
								    						Waiting on confirmation of start date from Jill 
								    					</div>
								    				</div>
								    			</div>
								    			<div class="dashbaord-list-view-subpannal">
								    				<div class="list-view-top">
								    					<div class="list-view-name">Eric Jones </div>
								    					<div class="list-view-same list-view-miles">
								    						<span><i class="fa fa-map-marker"></i></span>
								    						2241, Tawny Terr, Dublin
								    					</div>
								    					<div class="list-view-same view-student">
								    						Enrolled:  <span>4 out 6</span>
								    					</div>

								    					<div class="list-view-same view-student">
								    						New Requests:  <span>1</span>
								    					</div>

								    					<div class="list-view-same view-student">
								    						Waitlisted:   <span>0</span>
								    					</div>


								    				</div>
								    				<div class="list-view-bootm">
								    					<div class="list-view-math">
								    						<span><i class="fa fa-star"></i></span>
								    						Maths
								    					</div>
								    					<div class="list-view-same view-host">
								    						<span><i class="fa fa-calendar"></i></span>
								    						Next 16-May-17 11:00 AM
								    					</div>
								    					<div class="list-view-same view-host">
								    						<span><i class="fa fa-calendar"></i></span>
								    						Ends 15-Dec-17
								    					</div>
								    					<div class="list-view-same btm-waitng">
								    						<span><i class="fa fa-file-text"></i></span>
								    						Waiting on confirmation of start date from Jill 
								    					</div>
								    				</div>
								    			</div>
								    			<div class="dashbaord-list-view-subpannal">
								    				<div class="list-view-top">
								    					<div class="list-view-name">Eric Jones </div>
								    					<div class="list-view-same list-view-miles">
								    						<span><i class="fa fa-map-marker"></i></span>
								    						2241, Tawny Terr, Dublin
								    					</div>
								    					<div class="list-view-same view-student">
								    						Enrolled:  <span>4 out 6</span>
								    					</div>

								    					<div class="list-view-same view-student">
								    						New Requests:  <span>1</span>
								    					</div>

								    					<div class="list-view-same view-student">
								    						Waitlisted:   <span>0</span>
								    					</div>


								    				</div>
								    				<div class="list-view-bootm">
								    					<div class="list-view-math">
								    						<span><i class="fa fa-star"></i></span>
								    						Maths
								    					</div>
								    					<div class="list-view-same view-host">
								    						<span><i class="fa fa-calendar"></i></span>
								    						Next 16-May-17 11:00 AM
								    					</div>
								    					<div class="list-view-same view-host">
								    						<span><i class="fa fa-calendar"></i></span>
								    						Ends 15-Dec-17
								    					</div>
								    					<div class="list-view-same btm-waitng">
								    						<span><i class="fa fa-file-text"></i></span>
								    						Waiting on confirmation of start date from Jill 
								    					</div>
								    				</div>
								    				
								    			</div>
								    		
								    		</div>	
							    		</div>

							    	</div>
							    </div>
							  </div>

						</div>
	               </div>
	            </div>
	         </div>
	      </section>
      <?php include('footer.php'); ?>
      <script src="/tn/assets/js/jquery-1.11.3.min.js" type="text/javascript"></script> 
      <script src="/tn/assets/js/bootstrap.min.js" type="text/javascript"></script> 
      <script type="text/javascript" src="/tn/assets/js/owl.carousel.js"></script> 
      <script src="/assets/js/enscroll-0.6.2.min.js"></script> 
   </body>
</html>

