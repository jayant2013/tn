<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <title>Talentnook</title>
  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="/tn/assets/css/bootstrap.min.css" crossorigin="anonymous">
  <link rel="stylesheet" href="/tn/assets/css/font-awesome.css" crossorigin="anonymous">
  
  <!-- <link rel="stylesheet" href="assets/css/style.css" crossorigin="anonymous">-->
  <!-- <link rel="stylesheet" href="assets/css/responsive.css" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="/tn/assets/css/ui-screen.css" crossorigin="anonymous">
  <link rel="stylesheet" href="/tn/assets/css/responsive-ui-screen.css" crossorigin="anonymous">
 
 </head>
<body>

<?php include('header.php'); ?>

<?php include('home.php'); ?>

<?php include('footer.php'); ?>

<script src="/tn/assets/js/jquery-1.11.3.min.js" type="text/javascript"></script> 
<script src="/tn/assets/js/bootstrap.min.js" type="text/javascript"></script> 
<script type="text/javascript" src="/tn/assets/js/owl.carousel.js"></script> 
<script src="/assets/js/enscroll-0.6.2.min.js"></script> 
</body>
</html>


