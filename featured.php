

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Talentnook</title>
      <base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      <link rel="stylesheet" href="/tn/assets/css/bootstrap.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/font-awesome.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/ui-screen.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/tn/assets/css/responsive-ui-screen.css" crossorigin="anonymous">
   </head>
   <body>
      <?php include('header.php'); ?>
      <section class="feathured-banner header-botmsapce">
         <div class="fetured-banner-toptext">
            <div class="container">
               Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </div>   
         </div>
         <div class="container">
            <h2>Featured</h2>
            <div class="fetured-banner-box-inner">
               <div class="fetured-banner-box">
                  <div class="fetured-top-circleimg">
                  <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                  </div>
                  <h2>Finibus Bonorum</h2>
                  <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  <span class="list-view-tag">Dance</span>
                  <span class="list-view-tag">Getar</span>
                  <span class="list-view-tag">Yoga</span>
               </div>
                <div class="fetured-banner-box">
                  <div class="fetured-top-circleimg">
                  <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                  </div>
                  <h2>Finibus Bonorum</h2>
                  <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  <span class="list-view-tag">Dance</span>
                  <span class="list-view-tag">Getar</span>
                  <span class="list-view-tag">Yoga</span>
               </div>
                <div class="fetured-banner-box">
                  <div class="fetured-top-circleimg">
                  <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                  </div>
                  <h2>Finibus Bonorum</h2>
                  <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  <span class="list-view-tag">Dance</span>
                  <span class="list-view-tag">Getar</span>
                  <span class="list-view-tag">Yoga</span>
               </div>
                <div class="fetured-banner-box">
                  <div class="fetured-top-circleimg">
                  <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                  </div>
                  <h2>Finibus Bonorum</h2>
                  <div class="map-tooltip-rating">
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                     </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  <span class="list-view-tag">Dance</span>
                  <span class="list-view-tag">Getar</span>
                  <span class="list-view-tag">Yoga</span>
               </div>
            </div>
         </div>
      </section>
      <section class="dashbaord-content-section">
         <div class="container">
            <div class="row">
               <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="leftpannal">
                     <ul>
                        <li>
                           <a href="">
                           <i class="fa fa-home"></i>
                           Home
                           </a>
                        </li>
                        <li>
                           <a href=""> 
                           <img src="/tn/assets/images/left-pannal-logoicon.png">
                           My Talentnooks
                           </a>
                        </li>
                        <li>
                           <a href="">
                           <i class="fa fa-comments"></i>
                           Talentnook Forum</a>
                        </li>
                       <div class="leftpannal-subchild">
                          <ul>
                              <li>
                           <a href="">
                           <img src="/tn/assets/images/inbox-icon.png">
                           inbox</a>
                        </li>
                          </ul>
                       </div>
                     </ul>
                  </div>
                  <div class="dashbaord-right-section">
                     <div class="dashbaord-top-search">
                        <div class="dassearch-input"> 
                           <input type="text" placeholder="Search">
                           <i class="fa fa-search"></i>
                        </div>
                     </div>
                     <div class="dashbaord-bootom-sec">
                        <div class="dashbaord-msg-post">
                           <textarea placeholder="Post a Message"></textarea>
                           <button type="button" class="dash-postbtn">post</button>
                        </div>
                        <div class="dashbaord-post-inner">
                           <div class="dashbaord-post-details">
                              <div class="dashbaord-post-user">
                                 <span class="post-userimg">
                                 <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                                 </span>
                                 <span  class="post-username">Lina Park</span>
                              </div>
                              <div class="dashbaord-post-time">
                                 24 hours ago
                              </div>
                              <div class="dashbaord-post-sndpri">
                                 <a href="">
                                 send private
                                 </a>
                              </div>
                              <div class="dashbaord-post-rply">
                                 <a href="">
                                 <span>reply</span>
                                 <i class="fa fa-reply"></i>
                                 </a>
                              </div>
                           </div>
                           <p class="post-msg">
                              Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                              <a href="">see more</a>
                           </p>
                        </div>
                        <div class="dashbaord-post-inner">
                           <div class="dashbaord-post-details">
                              <div class="dashbaord-post-user">
                                 <span class="post-userimg">
                                 <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                                 </span>
                                 <span  class="post-username">Lina Park</span>
                              </div>
                              <div class="dashbaord-post-time">
                                 24 hours ago
                              </div>
                              <div class="dashbaord-post-sndpri">
                                 <a href="">
                                 send private
                                 </a>
                              </div>
                              <div class="dashbaord-post-rply">
                                 <a href="">
                                 <span>reply</span>
                                 <i class="fa fa-reply"></i>
                                 </a>
                              </div>
                           </div>
                           <p class="post-msg">
                              Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                              <a href="">see more</a>
                           </p>
                        </div>
                        <div class="dashbaord-post-subchild-inner">
                           <div class="post-show-more">
                              show more
                              <span>(10)</span>
                           </div>
                           <div class="dashbaord-post-inner">
                              <div class="dashbaord-post-details">
                                 <div class="dashbaord-post-user">
                                    <span class="post-userimg">
                                    <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                                    </span>
                                    <span  class="post-username">Lina Park</span>
                                 </div>
                                 <div class="dashbaord-post-time">
                                    24 hours ago
                                 </div>
                                 <div class="dashbaord-post-sndpri">
                                    <a href="">
                                    send private
                                    </a>
                                 </div>
                              </div>
                              <p class="post-msg">
                                 Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, dolor sit amet...
                                 <a href="">see more</a>
                              </p>
                           </div>
                           <div class="dashbaord-post-inner">
                              <div class="dashbaord-post-details">
                                 <div class="dashbaord-post-user">
                                    <span class="post-userimg">
                                    <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                                    </span>
                                    <span  class="post-username">Lina Park</span>
                                 </div>
                                 <div class="dashbaord-post-time">
                                    24 hours ago
                                 </div>
                                 <div class="dashbaord-post-sndpri">
                                    <a href="">
                                    send private
                                    </a>
                                 </div>
                              </div>
                              <p class="post-msg">
                                 Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, dolor sit amet...
                                 <a href="">see more</a>
                              </p>
                           </div>
                           <div class="post-msgtype">
                              <input type="" name="" placeholder="Message...">
                              <i class="fa fa-send"></i>
                           </div>
                        </div>
                        <div class="dashbaord-post-inner">
                           <div class="dashbaord-post-details">
                              <div class="dashbaord-post-user">
                                 <span class="post-userimg">
                                 <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                                 </span>
                                 <span  class="post-username">Lina Park</span>
                              </div>
                              <div class="dashbaord-post-time">
                                 24 hours ago
                              </div>
                              <div class="dashbaord-post-sndpri">
                                 <a href="">
                                 send private
                                 </a>
                              </div>
                              <div class="dashbaord-post-rply">
                                 <a href="">
                                 <span>reply</span>
                                 <i class="fa fa-reply"></i>
                                 </a>
                              </div>
                           </div>
                           <p class="post-msg">
                              Lorem ipsum dolo r sit amet, consectetur adipiscing elit. Fusce ac est augue. Praesent sed lectus vel mi vulputate consequat. Morbi vitae mollis justo, semper massa Fusce ac est augue. Praesent sed lectus vel mi vulputate Lorem ipsum dolor sit amet...
                              <a href="">see more</a>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('footer.php'); ?>
      <script src="/tn/assets/js/jquery-1.11.3.min.js" type="text/javascript"></script> 
      <script src="/tn/assets/js/bootstrap.min.js" type="text/javascript"></script> 
      <script type="text/javascript" src="/tn/assets/js/owl.carousel.js"></script> 
      <script src="/assets/js/enscroll-0.6.2.min.js"></script> 
   </body>
</html>

