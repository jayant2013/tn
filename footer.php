<footer class="footer">
<div class="foot-backtotop"><a href="#">Back to Top</a></div>
<div class="foot-cmslinks-inner">
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-3 col-md-3 cms-mobile">
				<div class="foot-cmslinks">
					<div class="cms-link-title">link</div>
					<ul>
						<li>
							<a href="">About us</a>
						</li>
						<li>
							<a href="">profile</a>
						</li>
						<li>
							<a href="">telentmaster profile</a>
						</li>
						<li>
							<a href="">dashboard</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 col-md-3 cms-mobile">
				<div class="foot-cmslinks">
					<div class="cms-link-title">other links</div>
					<ul>
						<li>
							<a href="">contact us</a>
						</li>
						<li>
							<a href="">sign up</a>
						</li>
						<li>
							<a href="">login</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 col-md-3 cms-mobile">
				<div class="foot-cmslinks">
					<div class="cms-link-title">Talentnook</div>
					<ul>
						<li>
							<a href="">terms and conditions</a>
						</li>
						<li>
							<a href="">privacy policy</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 col-md-3 cms-mobile">
				<div class="foot-logo-inner">
					<div class="foot-logo">
						<img class="img-responsive" src="/tn/assets/images/logo.png">
					</div>
					<button class="btn ask-btn">ask a question</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="foot-btm">
	<div class="container-fluid">
		<div class="foot-talentnook">@2017 Talentnook</div>
		<!-- <div class="new-social">
			<a href="">
				<i class="fa fa-facebook"></i>
			</a>
		</div> -->
		<div class="foot-btm-socail">
			<a href=""><img src="/tn/assets/images/facebook.png"></a>
			<a href=""><img src="/tn/assets/images/twitter-footer.png"></a>
			<a href=""><img src="/tn/assets/images/instagram-footer.png"></a>
			<a href=""><img src="/tn/assets/images/twits.png"></a>
		</div>
	</div>
</div>
</footer>






<!-- Modal Signin -->
<div class="modal fade common-modal" id="head-signin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Enter TalentNook</h4>
      </div>
      <div class="modal-body">
        <a href="" class="btn modal-fbbtn">
        	<i class="fa fa-facebook-square"></i>Sign In with Facebook
		</a>
        <a href="" class="btn modal-googlebtn">
        	<i class="fa fa-google" aria-hidden="true"></i>Sign In with Google
        </a>
        <div class="modal-seprator-or">
        	<span>or</span>
        </div>
        <div class="modal-input-inner">
        	<input type="text" name="" class="modal-input" placeholder="User Name">
        	<span><i class="fa fa-user"></i></span>
        </div>
        <div class="modal-input-inner">
        	<input type="password" name="" class="modal-input" placeholder="Password">
        	<span><i class="fa fa-lock"></i></span>
        </div>
        <div class="home-skills-checkbox modal-checkbox">
            <input class="homecheckbox" name="r1" id="r1" type="checkbox">
            <label for="r1"> Remember me  <span></span> </label>
         </div>
        <button class="btn modal-signbtn">Sign In</button>
        <a href="" class="modal-forgotpass">Forgot Password?</a>
      </div>
      <div class="modal-footer">
        <div class="modal-footer-clickhere">Cleck here <a href="">Join Talentnook</a></div>
      </div>
    </div>
  </div>
</div>




<!-- Modal Sign up-->
<div class="modal fade" id="head-signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sign up</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>




