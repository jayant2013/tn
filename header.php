

<header class="header">
   <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
            <img class="img-responsive" src="/tn/assets/images/logo.png">
            </a>
         </div>
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
               

               <!-- Before login start-->
               <li><a href="#" data-toggle="modal" data-target="#head-signin">Sign In</a></li>
               <span class="head-menu-separator"></span>
               <li><a href="#" data-toggle="modal" data-target="#head-signup">Sign up</a></li>
               <span class="head-menu-separator"></span>
               <li><a href="#">Faq</a></li>
               <!-- Before login end-->

				<!-- After login start-->
               <li>
                  <div class="head-notifi-inner">
                     <a href="">
                     <i class="fa fa-comment"></i>
                     <span class="head-notific">5</span>
                     </a>
                  </div>
               </li>

               <span class="head-menu-separator"></span>

               <li class="dropdown head-rightdropdown last-child">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <span class="head-userimg">
                  <img class="img-responsive" src="tn/assets/images/prof-1.jpg">
                  </span>
                  <span class="head-user-name">michaela vesela</span>
                  <span class="drop-arrow">
                  <i class="fa drop-arrow-icon"></i>
                  </span>
                  </a>
                  <ul class="dropdown-menu">
                     <li><a href="#">Profile</a></li>
                     <li><a href="#">Talentmaster Profile</a></li>
                     <li><a href="#">Logout</a></li>
                  </ul>
               </li>

               <!-- After login end-->

            </ul>
         </div>
         <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
   </nav>
</header>

