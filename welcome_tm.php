<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <title>Talentnook</title>
  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="/tn/assets/css/bootstrap.min.css" crossorigin="anonymous">
  <link rel="stylesheet" href="/tn/assets/css/font-awesome.css" crossorigin="anonymous">
  
  <!-- <link rel="stylesheet" href="assets/css/style.css" crossorigin="anonymous">-->
  <!-- <link rel="stylesheet" href="assets/css/responsive.css" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="/tn/assets/css/ui-screen.css" crossorigin="anonymous">
  <link rel="stylesheet" href="/tn/assets/css/responsive-ui-screen.css" crossorigin="anonymous">
 
 </head>
<body>

<?php include('header.php'); ?>

<section class="header-botmsapce">
  <div class="container-fluid welcom-container">
    <div class="welcome-bx">
      <div class="welcome-img"><img src="tn/assets/images/girls-group.jpg" class="img-responsive"></div>
    </div><!-- welcome-bx -->
  </div>
</section><!-- welcome -->
<section class="wel-yellow-back-color-tm">
  <div class="container">
    <div class="row">
      <div class="wel-color-inner">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="wel-color-inner-in">
            <div class="wel-color-tx">welcome to</div>
            <div class="wel-color-text">your talentnook community!</div>
            <div class="wel-color-para">
              Your email not verified yet. Please verify, We have sent you verification link in your mail.
            </div>
            <div class="wel-color-para">
              Your address not verified yet. Please contact to Talentnook Admin.
            </div>
            <div class="wel-color-para">Your profile is currently under review. We will let you know as soon as you are approved and ready to start teaching.</div>
            <div class="welcome-button-bx">
              <button type="button" class="wel-but">Create Talentnook</button>
              <button type="button" class="wel-but">Go to your Dashboard</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section><!-- wel-yellow-back-color -->
<section class="fea-stu-bx">
  <div class="container-fluid welcom-container">
     <div class="fea-stu-bx-in">
        <div class="fea-stu-img"><img src="tn/assets/images/welcome-foot.jpg" class="img-responsive"></div>
     </div> 
  </div>
</section><!-- fea-stu-bx -->

<?php include('footer.php'); ?>

<script src="/tn/assets/js/jquery-1.11.3.min.js" type="text/javascript"></script> 
<script src="/tn/assets/js/bootstrap.min.js" type="text/javascript"></script> 
<script type="text/javascript" src="/tn/assets/js/owl.carousel.js"></script> 
<script src="/tn/assets/js/enscroll-0.6.2.min.js"></script> 
</body>
</html>


